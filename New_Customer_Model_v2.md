
# Introduction

The business needs to know which new customers have tendency to redeem the items thay have loaned.

This to optimize the percentage of money that will be lent to the clients according to the articles that take pawn.

For the business, it is considered as a **New Customer** a person who has **0, 1 o 2 loans** for **General Merchandise**

So, _in this document we will present the development of a model that gives the probability of redeem to each new customer_.

This is the second iteration of this model, in this model we are going to add the **marginalization** of each customer.

The **marginalization** can be:
 - Very Low
 - Low
 - Medium
 - High
 - Very High

This data is from a table that has:
 - Zip code
 - Marginalization

So, in the _cleaninig data_, we are only going to keep customers with a good _zip code_

## DB & Tables used

For the model, the features were created from **EzPawnInfo**, using the tables:

 - LoanSummary
 - LoanTable
 - CustomerTable
 - NewCategoryTable

## Data specs

The data were created following this order:

 - Customers with their first loan in 2018 or 2019
 - From Mexico
 - Having as **LastAction** in all their posible loans "Drop Loan" or "Loan Redemption"

To differentiate the data that will be considered as independent (features) and dependent (y) variables, these rules were followed:
 - **Customers with 1 loan:**
  - **Features**: All information from the unique loan they have, considering drops and redemptions in zero.
  - **y**: The "lastAction" that they did in this unique loan
 - **Customers with 2 or 3 loans:**
  - **Features**: All information without considering their last loan
  - **y**: The "lastAction" of their last loan

## Features

The features used are divided in this categories:
 - **Loan & Customer Features:** Age, State of residence, Gender, Original Loans, Drops, Loan Redemptions, Renewals, etc
 - **Store Features:** State of the stores each customer made loans, number of competitions stores near each store, loans in stores in the same state of residence of the customers
 - **Categories features:** Number of items loaned for each category
 - **MonthlyCharge features:** From the LoanTable
 - **Hour, day & Month:** From the LoanSummaryTable
 - **Marginalization**

## Additional Data

 1. We are going to use a table with the distance between all our stores and all the mexican pawn stores, to know how many competitions stores are close to our stores.
 2. The store mexican distribution (From Luis David)
 3. Marginalization for each zip code

# Libraries


```python
import pandas as pd
import numpy as np
from analytics import *
import pickle
from dfply import *
from plotnine import *
import datetime as dt
from mizani.formatters import percent_format
from mizani.formatters import comma_format
import matplotlib.pyplot as plt
import lightgbm as lgb
from sklearn.cross_validation import train_test_split
from sklearn.metrics import (mean_squared_error,mean_absolute_error,
                             r2_score,explained_variance_score, roc_auc_score,
                            classification_report, confusion_matrix,
                            roc_curve, accuracy_score)
import sklearn
import sklearn.metrics as metrics
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
```

# Querys

Querys used to create the data:
 - Features
 - Y

## Loan & Customer Features

    --------------------------------------------
    --- LoanSummary & CustomerTable Features ---
    --------------------------------------------


    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    -----------------------------
    -- LoanSummary table, only with the data of first Customers
    LOANSUMMARY_1_loan AS (
    SELECT
     *
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
     YEAR(ls.OriginDate) >= 2018                                  AND
     ls.PrimaryLoan IS NOT NULL                                   AND
     ls.LoanCountry = 'Mex'
    ),

    -------------------------------
    -- Features for first Customers
    Features_1_Loan AS (
    SELECT
     LSF.*,
     CASE WHEN cust.Female = 'T' THEN 'Female' ELSE 'Male' END AS Gender,
     cust.City,
     cust.state,
     cust.birth_date,
     cust.zip
    FROM(
      SELECT
       ls.Customer AS Customer,
       0           AS total_loans,
       0           AS total_loan_redemption,
       0           AS total_drop_loan,
       0           AS total_Originalloan_loan_redemption,
       0           AS total_Originalloan_drop_loan, 

       1    AS total_items,
       1    AS avg_items_per_loan,
       0    AS sd_items_per_loan,
       1    AS max_items_per_loan, 

       0 AS total_originalloan,
       0 AS avg_originalloan_per_loan,
       0               AS sd_originalloan_per_loan, 

       0 AS total_renewals,
       0 AS avg_renewals,
       0 AS max_renewals,
       0 AS min_renewals, 

       0 AS total_RenewalDollars,
       0 AS avg_RenewalDollars,
       0 AS sd_RenewalDollars

      FROM
       LOANSUMMARY_1_loan ls) LSF
    LEFT JOIN
     ezpawninfo.dbo.CustomerTable cust
    ON
     cust.CustomerID=LSF.Customer
    ),

    -----------------------------
    -- LoanSummary table, only with the data of Customers with 2 or 3 loans
    -- To this Customers, the lastaction of their lastloan is going to be the "y" to predict, so the features are going to be of the past loans
    LOANSUMMARY_morethan1_loan AS (
    SELECT
     *
    FROM(
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      ezpawninfo.dbo.LoanSummary ls
     WHERE
      Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
      ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
      YEAR(ls.OriginDate) >= 2018                                  AND
      ls.PrimaryLoan IS NOT NULL                                   AND
      ls.LoanCountry = 'Mex') a
    WHERE
     rank > 1
    ),

    -------------------------------
    -- Features for Customers with 2 or 3 loans
    Features_morethan1_loan AS (
    SELECT
     LSF.*,
     CASE WHEN cust.Female = 'T' THEN 'Female' ELSE 'Male' END AS Gender,
     cust.City,
     cust.state,
     cust.birth_date,
     cust.zip
    FROM(
      SELECT
       ls.Customer                                         AS Customer,
       COUNT(1)                                            AS total_loans, 

       SUM(CASE
        WHEN ls.lastAction = 'Loan Redemption'
        THEN 1 ELSE 0 END)                             AS total_loan_redemption,
       SUM(CASE
        WHEN ls.lastAction = 'Drop Loan'
        THEN 1 ELSE 0 END)                             AS total_drop_loan, 

       SUM(CASE
        WHEN ls.lastAction = 'Loan Redemption'
        THEN ls.OriginalLoan ELSE 0 END)               AS total_Originalloan_loan_redemption,
       SUM(CASE
        WHEN ls.lastAction = 'Drop Loan'
        THEN ls.OriginalLoan ELSE 0 END)               AS total_Originalloan_drop_loan, 
   
       SUM(ls.items)                                       AS total_items,
       AVG(CAST(ls.items AS FLOAT))                        AS avg_items_per_loan,
       STDEVP(ls.items)                                    AS sd_items_per_loan,
       MAX(ls.items)                                       AS max_items_per_loan, 
   
       SUM(ls.OriginalLoan)                                AS total_originalloan,
       AVG(ls.OriginalLoan)                                AS avg_originalloan_per_loan,
       STDEVP(ls.OriginalLoan)                             AS sd_originalloan_per_loan,

       SUM(ls.Renewals)                                    AS total_renewals,
       AVG(ls.Renewals)                                    AS avg_renewals,
       MAX(ls.Renewals)                                    AS max_renewals,
       MIN(ls.Renewals)                                    AS min_renewals, 

       SUM(ls.RenewalDollars)                              AS total_RenewalDollars,
       AVG(ls.RenewalDollars)                              AS avg_RenewalDollars,
       STDEVP(ls.RenewalDollars)                           AS sd_RenewalDollars
      FROM
       LOANSUMMARY_morethan1_loan ls
      GROUP BY
       ls.Customer) LSF
    LEFT JOIN
     ezpawninfo.dbo.CustomerTable cust
    ON
     cust.CustomerID=LSF.Customer
    ),

    ---------------
    -- Create features, making a union all of the 1 loan customers features and the more than 1 loan customer features
    Features AS (
    SELECT
     *
    FROM
     Features_1_Loan
    UNION ALL
    SELECT
     *
    FROM
     Features_morethan1_loan)

    ---------------
    -- Print the result of the query to save as csv
    SELECT * FROM Features



    -------------------
    --   Validations --
    -------------------
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM New_Customers_2018_morethan1_loan --93,209
    --SELECT COUNT(1) FROM Features_morethan1_loan --93,209


    --SELECT COUNT(1) FROM New_Customers_2018_1_loan --287,564
    --SELECT COUNT(1) FROM Features_1_Loan --287,564

    --SELECT COUNT(1) FROM New_Customers_2018_1_loan WHERE Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) --0
    --SELECT COUNT(1) FROM New_Customers_2018_morethan1_loan WHERE Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) --0

## Categories Features

    -----------------------------------------------------
    --                  Category Features              --
    -----------------------------------------------------

    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    -----------------------------
    -- LoanSummary table, only with the data of first Customers
    LOANSUMMARY_1_loan AS (
    SELECT
     *
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
     YEAR(ls.OriginDate) >= 2018                                  AND
     ls.PrimaryLoan IS NOT NULL                                   AND
     ls.LoanCountry = 'Mex'
    ),

    --------------------------
    -- Category features of 1 loan customers

    Category_features_1_loan AS (
    SELECT
     aux.Customer,
     SUM(CASE WHEN cat.Secondary = 'Bulk'                 THEN 1 ELSE 0 END) AS Bulks,
     SUM(CASE WHEN cat.Secondary = 'Cameras & Optics'     THEN 1 ELSE 0 END) AS Cameras_Optics,
     SUM(CASE WHEN cat.Secondary = 'Earrings'             THEN 1 ELSE 0 END) AS Earrings,
     SUM(CASE WHEN cat.Secondary = 'Electronics'          THEN 1 ELSE 0 END) AS Electronics,
     SUM(CASE WHEN cat.Secondary = 'Firearms'             THEN 1 ELSE 0 END) AS Firearms,
     SUM(CASE WHEN cat.Secondary = 'Household & Office'   THEN 1 ELSE 0 END) AS Household_Office,
     SUM(CASE WHEN cat.Secondary = 'Jewelry'              THEN 1 ELSE 0 END) AS Jewelry,
     SUM(CASE WHEN cat.Secondary = 'Musical Gear'         THEN 1 ELSE 0 END) AS Musical_Gear,
     SUM(CASE WHEN cat.Secondary = 'Quantity Based Items' THEN 1 ELSE 0 END) AS Quantity_Based_Items,
     SUM(CASE WHEN cat.Secondary = 'Sports & Recreation'  THEN 1 ELSE 0 END) AS Sports_Recreation,
     SUM(CASE WHEN cat.Secondary = 'Tools & Garden'       THEN 1 ELSE 0 END) AS Tools_Garden,
     SUM(CASE WHEN cat.Secondary = 'Transportation'       THEN 1 ELSE 0 END) AS Transportation,
     SUM(CASE WHEN cat.Secondary = 'Jewelry'              THEN 1 ELSE 0 END) AS ind_Jewelry
    FROM(
     SELECT
      LS.Customer   AS Customer,
      it.CategoryID AS CategoryID
     FROM
      LOANSUMMARY_1_loan LS
     LEFT JOIN
      dbo.ItemTable it
     ON
      it.LoanID=LS.PrimaryLoan) aux
    LEFT JOIN dbo.NewCategoryTable cat ON cat.CategoryID=aux.CategoryID
    GROUP BY
     aux.Customer
    ),

    -----------------------------
    -- LoanSummary table, only with the data of Customers with 2 or 3 loans
    -- To this Customers, the lastaction of their lastloan is going to be the "y" to predict, so the features are going to be of the past loans
    LOANSUMMARY_morethan1_loan AS (
    SELECT
     *
    FROM(
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      ezpawninfo.dbo.LoanSummary ls
     WHERE
      Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
      ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
      YEAR(ls.OriginDate) >= 2018                                  AND
      ls.PrimaryLoan IS NOT NULL                                   AND
      ls.LoanCountry = 'Mex') a
    WHERE
     rank > 1
    ),

    --------------------------
    -- Category features of more than 1 loan customers
    Category_features_morethan1_loan AS (
    SELECT
     aux.Customer,
     SUM(CASE WHEN cat.Secondary = 'Bulk'                 THEN 1 ELSE 0 END) AS Bulks,
     SUM(CASE WHEN cat.Secondary = 'Cameras & Optics'     THEN 1 ELSE 0 END) AS Cameras_Optics,
     SUM(CASE WHEN cat.Secondary = 'Earrings'             THEN 1 ELSE 0 END) AS Earrings,
     SUM(CASE WHEN cat.Secondary = 'Electronics'          THEN 1 ELSE 0 END) AS Electronics,
     SUM(CASE WHEN cat.Secondary = 'Firearms'             THEN 1 ELSE 0 END) AS Firearms,
     SUM(CASE WHEN cat.Secondary = 'Household & Office'   THEN 1 ELSE 0 END) AS Household_Office,
     SUM(CASE WHEN cat.Secondary = 'Jewelry'              THEN 1 ELSE 0 END) AS Jewelry,
     SUM(CASE WHEN cat.Secondary = 'Musical Gear'         THEN 1 ELSE 0 END) AS Musical_Gear,
     SUM(CASE WHEN cat.Secondary = 'Quantity Based Items' THEN 1 ELSE 0 END) AS Quantity_Based_Items,
     SUM(CASE WHEN cat.Secondary = 'Sports & Recreation'  THEN 1 ELSE 0 END) AS Sports_Recreation,
     SUM(CASE WHEN cat.Secondary = 'Tools & Garden'       THEN 1 ELSE 0 END) AS Tools_Garden,
     SUM(CASE WHEN cat.Secondary = 'Transportation'       THEN 1 ELSE 0 END) AS Transportation,
     SUM(CASE WHEN cat.Secondary = 'Jewelry'              THEN 1 ELSE 0 END) AS ind_Jewelry
    FROM(
     SELECT
      LS.Customer   AS Customer,
      it.CategoryID AS CategoryID
     FROM
      LOANSUMMARY_morethan1_loan LS
     LEFT JOIN
      dbo.ItemTable it
     ON
      it.LoanID=LS.PrimaryLoan) aux
    LEFT JOIN dbo.NewCategoryTable cat ON cat.CategoryID=aux.CategoryID
    GROUP BY
     aux.Customer
    ),

    ----------------------
        -- Union all of the 1 loan and more than 1 loan customers features
    Category_Features AS (
    SELECT
     *
    FROM
     Category_features_1_loan
    UNION ALL
    SELECT
     *
    FROM
     Category_features_morethan1_loan
    )

    ---------------
    -- Print the result of the query to save as csv
    SELECT * FROM Category_Features

    -------------------
    --   Validations --
    -------------------
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM New_Customers_2018_morethan1_loan --93,209
    --SELECT COUNT(1) FROM Category_features_morethan1_loan --93,209


    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM New_Customers_2018_1_loan --287,564
    --SELECT COUNT(1) FROM Category_features_1_loan --287,564

    --SELECT COUNT(1) FROM Category_Features --380,773 = 287,564 + 93,209

## MonthlyCharges Features

    -------------------------
    ---  Monthly Charge   ---
    -------------------------

    -- Monthly Charges features from the LoanTable

    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    -----------------------------
    -- LoanSummary table, only with the data of first Customers
    LOANSUMMARY_1_loan AS (
    SELECT
     *
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
     YEAR(ls.OriginDate) >= 2018                                  AND
     ls.PrimaryLoan IS NOT NULL                                   AND
     ls.LoanCountry = 'Mex'
    ),

    ------------------------------------
    -- Features for 1 loan Customers
    MonthlyCharge_1_loan AS (
    SELECT
     lt.Customer,
    
     0 AS total_MonthlyCharge,
     0 AS avg_MonthlyCharge,
     0                     AS sd_MonthlyCharge,
     0 AS max_MonthlyCharge,
     0 AS min_MonthlyCharge
    FROM
     dbo.LoanTable lt
    WHERE
     CONCAT(lt.PrimaryLoan,lt.Customer) IN (SELECT CONCAT(PrimaryLoan,Customer) FROM LOANSUMMARY_1_loan)
    GROUP BY
     lt.Customer
    ),

    -----------------------------
    -- LoanSummary table, only with the data of Customers with 2 or 3 loans
    -- To this Customers, the lastaction of their lastloan is going to be the "y" to predict, so the features are going to be of the past loans
    LOANSUMMARY_morethan1_loan AS (
    SELECT
     *
    FROM(
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      ezpawninfo.dbo.LoanSummary ls
     WHERE
      Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
      ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
      YEAR(ls.OriginDate) >= 2018                                  AND
      ls.PrimaryLoan IS NOT NULL                                   AND
      ls.LoanCountry = 'Mex') a
    WHERE
     rank > 1
    ),

    ------------------------------------
    -- Features for mora thean 1 loan Customers
    MonthlyCharge_morethan1_loan AS (
    SELECT
     lt.Customer,

     SUM(lt.MonthlyCharge)    AS total_MonthlyCharge,
     AVG(lt.MonthlyCharge)    AS avg_MonthlyCharge,
     STDEVP(lt.MonthlyCharge) AS sd_MonthlyCharge,
     MAX(lt.MonthlyCharge)    AS max_MonthlyCharge,
     MIN(lt.MonthlyCharge)    AS min_MonthlyCharge
    FROM
     (SELECT
       *
      FROM
       dbo.LoanTable
      WHERE
       YEAR(OriginDate) >= 2018 AND
       CONCAT(PrimaryLoan,Customer,OriginDate) IN (SELECT CONCAT(PrimaryLoan,Customer,OriginDate) FROM LOANSUMMARY_morethan1_loan)
     ) lt
    GROUP BY
     lt.Customer
    ),

    --------------------------------------
    --Union of features

    Monthly_features AS (
    SELECT
     *
    FROM
     MonthlyCharge_1_loan
    UNION ALL
    SELECT
     *
    FROM
     MonthlyCharge_morethan1_loan
    )


    ---------------
    -- Print the result of the query to save as csv
    SELECT * FROM Monthly_features

    -------------------
    --   Validations --
    -------------------
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM MonthlyCharge_1_loan --287,564
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM MonthlyCharge_morethan1_loan --93,209

    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM Monthly_features --380,773 = 287,564 + 93,209

## Store table

The store table has all the stores which a customer had loan, with a column that specify the ranking of the visits per store

    ------------------------------------------
    --         Historic Store Loans         --
    ------------------------------------------

    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    ------------------------------
    -- LoanSummary table, only with the data of first Customers, with the stores for each loan
    LOANSUMMARY_1_loan AS (
    SELECT
     *
    FROM
     dbo.LoanSummary ls
    WHERE
     Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
     YEAR(ls.OriginDate) >= 2018                                  AND
     ls.PrimaryLoan IS NOT NULL                                   AND
     ls.LoanCountry = 'Mex'
     ),

    -------------------------------
    -- Historic Stores for first loan customers
    stores_1_loan AS (
    SELECT
     DISTINCT
      lt.Customer,
      lt.PrimaryLoan,
      lt.store,
      lt.OriginDate,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate DESC) AS rank
    FROM
     dbo.LoanTable lt
    WHERE
     CONCAT(lt.PrimaryLoan,lt.Customer) IN (SELECT CONCAT(LS.PrimaryLoan,LS.Customer) FROM LOANSUMMARY_1_loan LS)
    ),

    ------------------------------
    -- LoanSummary table, only with the data of Customers with 1 or 2 loans, with the stores for each loan
    LOANSUMMARY_morethan1_loan AS (
    SELECT
     *
    FROM(
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      ezpawninfo.dbo.LoanSummary ls
     WHERE
      Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
      ls.lastAction IN ('Loan Redemption','Drop Loan')             AND
      YEAR(ls.OriginDate) >= 2018                                  AND
      ls.PrimaryLoan IS NOT NULL                                   AND
      ls.LoanCountry = 'Mex') a
    WHERE
     rank > 1
    ),

    -------------------------------
    -- Historic Stores for more than 1 loan customers
    stores_morethan1_loan AS (
    SELECT
     DISTINCT
      lt.Customer,
      lt.PrimaryLoan,
      lt.store,
      lt.OriginDate,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate DESC) AS rank
    FROM
     dbo.LoanTable lt
    WHERE
     CONCAT(lt.PrimaryLoan,lt.Customer) IN (SELECT CONCAT(LS.PrimaryLoan,LS.Customer) FROM LOANSUMMARY_morethan1_loan LS)
    ),

    -------------------
    -- Union of the features
    stores AS (
    SELECT
     *
    FROM
     stores_1_loan
    UNION ALL
    SELECT
     *
    FROM
     stores_morethan1_loan
    )


    -----------------
    -- Print data to save as CSV
    SELECT * FROM stores ORDER BY Customer, rank



    -------------------
    --   Validations --
    -------------------
    --SELECT COUNT(1) FROM New_Customers_2018_1_loan --287,564
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM stores_1_loan --287,564

    --SELECT COUNT(1) FROM New_Customers_2018_morethan1_loan --93,209
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM stores_morethan1_loan --93,209

    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM stores --380,773 = 287,564+93,209

## Hour, day & Month Features

From the **LoanSummary** table, we create somo features using the OriginDate from the last loan for each Customer

The hours are going to be divided in:
  - **early_morning:** loans made between 0-8 hours
  - **morning:** loans made between 9-12 hours
  - **meal:** loans made between 1-3 hours
  - **afternoon:** loans made between 4-20 hours
  - **night:** loans made between 21-23 hours

The days are going to be divided in:
  - **Weekday:** Monday-Thursday
  - **Friday**
  - **Weekend:** Saturday & Sunday
  - **Payday:** Days in 14,15,15 or 29,30,31
  - **Not Payday:** Days not in 14,15,15 or 29,30,31

    ---------------------------------------------
    --      Hour, Day & Month - Last Loan      --
    ---------------------------------------------

    -- We want to know the hour, day and month of the last loan a customer made.

    -- The hours are going to be divided in:
    --  - early_morning: loans made between 0-8 hours
    --  - morning: loans made between 9-12 hours
    --  - meal: loans made between 1-3 hours
    --  - afternoon: loans made between 4-20 hours
    --  - night: loans made between 21-23 hours

    -- The days are going to be divided in:
    --  - Weekday: Monday-Thursday
    --  - Friday
    --  - Weekend: Saturday & Sunday
    --  - Payday: Days in 14,15,15 or 29,30,31
    --  - Not Payday: Days not in 14,15,15 or 29,30,31



    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    --------------------------
    -- hour, day and month features for 1 loan customers
    hdm_1_loan AS (
    SELECT
     Customer,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (0,1,2,3,4,5,6,7,8) THEN 1 ELSE 0 END AS early_morning,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (9,10,11,12)        THEN 1 ELSE 0 END AS morning,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (13,14,15)          THEN 1 ELSE 0 END AS meal,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (16,17,18,19,20)    THEN 1 ELSE 0 END AS afternoon,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (21,22,23)          THEN 1 ELSE 0 END AS night, 

     CASE WHEN DATEPART(DW, OriginDate) IN (2,3,4,5) THEN 1 ELSE 0 END AS weekday,
     CASE WHEN DATEPART(DW, OriginDate) IN (6)       THEN 1 ELSE 0 END AS friday,
     CASE WHEN DATEPART(DW, OriginDate) IN (1,7)     THEN 1 ELSE 0 END AS weekend, 

     CASE WHEN DATEPART(DAY, OriginDate)     IN (14,15,16,29,30,31,1) THEN 1 ELSE 0 END AS payday,
     CASE WHEN DATEPART(DAY, OriginDate) NOT IN (14,15,16,29,30,31,1) THEN 1 ELSE 0 END AS not_payday, 

     CASE WHEN DATEPART(MONTH, OriginDate) = 1   THEN 1 ELSE 0 END AS january,
     CASE WHEN DATEPART(MONTH, OriginDate) = 2   THEN 1 ELSE 0 END AS february,
     CASE WHEN DATEPART(MONTH, OriginDate) = 3   THEN 1 ELSE 0 END AS march,
     CASE WHEN DATEPART(MONTH, OriginDate) = 4   THEN 1 ELSE 0 END AS april,
     CASE WHEN DATEPART(MONTH, OriginDate) = 5   THEN 1 ELSE 0 END AS may,
     CASE WHEN DATEPART(MONTH, OriginDate) = 6   THEN 1 ELSE 0 END AS june,
     CASE WHEN DATEPART(MONTH, OriginDate) = 7   THEN 1 ELSE 0 END AS july,
     CASE WHEN DATEPART(MONTH, OriginDate) = 8   THEN 1 ELSE 0 END AS august,
     CASE WHEN DATEPART(MONTH, OriginDate) = 9   THEN 1 ELSE 0 END AS september,
     CASE WHEN DATEPART(MONTH, OriginDate) = 10  THEN 1 ELSE 0 END AS october,
     CASE WHEN DATEPART(MONTH, OriginDate) = 11  THEN 1 ELSE 0 END AS november,
     CASE WHEN DATEPART(MONTH, OriginDate) = 12  THEN 1 ELSE 0 END AS december
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary ls
     WHERE
     ls.Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')                AND
     YEAR(ls.OriginDate) >= 2018                                     AND
     ls.PrimaryLoan IS NOT NULL                                      AND
     ls.LoanCountry = 'Mex'           
    ) aux
    WHERE
     rank = 1
    ),

    --------------------------
    -- hour, day and month features for more than 1 loan customers
    hdm_morethan1_loan AS (
    SELECT
     Customer,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (0,1,2,3,4,5,6,7,8) THEN 1 ELSE 0 END AS early_morning,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (9,10,11,12)        THEN 1 ELSE 0 END AS morning,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (13,14,15)          THEN 1 ELSE 0 END AS meal,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (16,17,18,19,20)    THEN 1 ELSE 0 END AS afternoon,
     CASE WHEN DATEPART(HOUR, OriginDate) IN (21,22,23)          THEN 1 ELSE 0 END AS night, 

     CASE WHEN DATEPART(DW, OriginDate) IN (2,3,4,5) THEN 1 ELSE 0 END AS weekday,
     CASE WHEN DATEPART(DW, OriginDate) IN (6)       THEN 1 ELSE 0 END AS friday,
     CASE WHEN DATEPART(DW, OriginDate) IN (1,7)     THEN 1 ELSE 0 END AS weekend, 

     CASE WHEN DATEPART(DAY, OriginDate)     IN (14,15,16,29,30,31,1) THEN 1 ELSE 0 END AS payday,
     CASE WHEN DATEPART(DAY, OriginDate) NOT IN (14,15,16,29,30,31,1) THEN 1 ELSE 0 END AS not_payday, 

     CASE WHEN DATEPART(MONTH, OriginDate) = 1   THEN 1 ELSE 0 END AS january,
     CASE WHEN DATEPART(MONTH, OriginDate) = 2   THEN 1 ELSE 0 END AS february,
     CASE WHEN DATEPART(MONTH, OriginDate) = 3   THEN 1 ELSE 0 END AS march,
     CASE WHEN DATEPART(MONTH, OriginDate) = 4   THEN 1 ELSE 0 END AS april,
     CASE WHEN DATEPART(MONTH, OriginDate) = 5   THEN 1 ELSE 0 END AS may,
     CASE WHEN DATEPART(MONTH, OriginDate) = 6   THEN 1 ELSE 0 END AS june,
     CASE WHEN DATEPART(MONTH, OriginDate) = 7   THEN 1 ELSE 0 END AS july,
     CASE WHEN DATEPART(MONTH, OriginDate) = 8   THEN 1 ELSE 0 END AS august,
     CASE WHEN DATEPART(MONTH, OriginDate) = 9   THEN 1 ELSE 0 END AS september,
     CASE WHEN DATEPART(MONTH, OriginDate) = 10  THEN 1 ELSE 0 END AS october,
     CASE WHEN DATEPART(MONTH, OriginDate) = 11  THEN 1 ELSE 0 END AS november,
     CASE WHEN DATEPART(MONTH, OriginDate) = 12  THEN 1 ELSE 0 END AS december
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary ls
     WHERE
     ls.Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')                AND
     YEAR(ls.OriginDate) >= 2018                                     AND
     ls.PrimaryLoan IS NOT NULL                                      AND
     ls.LoanCountry = 'Mex'           
    ) aux
    WHERE
     rank = 1
    ),

    --------------------
    -- Union of the features for both types of customes
    hdm AS (
    SELECT
     *
    FROM
     hdm_1_loan
    UNION ALL
    SELECT
     *
    FROM
     hdm_morethan1_loan
    )

    ----------------
    -- Print the results to save as CSV
    SELECT * FROM hdm

## Y table

Create the lastAction from the last loan per customer

    ---------------------------------------
    --      Y Variable: Last Action      --
    ---------------------------------------

    --As we see when the features were created, the customers with 1 loan were trated as completely new customers, i.e., their features are
    --from that unique loan that they have done, that means that the "y" to fit the model is the LastAction of that unique lone

    --And for customers with 2 or 3 loans, the goal is to fit a model using features of their past loans to predict their LastAction of the last loan


    WITH
    -- Customers with first loan in 2018
    Customers_2018 AS (
    SELECT
     DISTINCT
     cust.CustomerID AS CustomerID
    FROM
     ezpawninfo.dbo.CustomerTable cust
    WHERE
     YEAR(cust.FirstLoanDt) >= 2018
    ),

    -- Customers with first loan in 2018 and maximum 3 loans
    New_Customers_2018 AS (
    SELECT
     ls.Customer AS Customer,
     COUNT(1)    AS n
    FROM
     ezpawninfo.dbo.LoanSummary ls
    WHERE
     ls.Customer IN (SELECT CustomerID FROM Customers_2018) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')       AND
     YEAR(ls.OriginDate) >= 2018                            AND
     ls.PrimaryLoan IS NOT NULL                             AND
     ls.LoanCountry = 'Mex'
    GROUP BY
     ls.Customer
    HAVING
     COUNT(1) <= 3
    ),

    -- To train de model, the customers are going to be divided in two:
    --  1. Customers with 1 loan: For these customers, the "y" is going to be the LastAction, because we supposed they don't have other loans
    --  2. Customers with 2 or 3 loans: For these customers, the model will predict the LastAction of the Last Loan:
    --       *Customer with 2 loans: Features of the first loan to predict the LastAction of the second loan
    --       *Customer with 3 loans: Features of the first & second loan to predict the LastAction of the third loan
    --------------------------------
    -- Customers with 1 loan
    New_Customers_2018_1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n = 1
    ),

    --------------------------------
    -- Customers with 2 or 3 loans
    New_Customers_2018_morethan1_loan AS (
    SELECT
     Customer
    FROM
     New_Customers_2018
    WHERE
     n > 1
    ),

    --------------------------
    -- Y for 1 loan customers
    y_1_loan AS (
    SELECT
     Customer,
     lastAction
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary ls
     WHERE
     ls.Customer IN (SELECT Customer FROM New_Customers_2018_1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')                AND
     YEAR(ls.OriginDate) >= 2018                                     AND
     ls.PrimaryLoan IS NOT NULL                                      AND
     ls.LoanCountry = 'Mex'           
    ) aux
    WHERE
     rank = 1
    ),

    --------------------------
    -- Y for more than 1 loan customers
    y_morethan1_loan AS (
    SELECT
     Customer,
     lastAction
    FROM
    (
     SELECT
      *,
      ROW_NUMBER() OVER (PARTITION BY Customer ORDER BY OriginDate ASC) AS rank
     FROM
      dbo.LoanSummary ls
     WHERE
     ls.Customer IN (SELECT Customer FROM New_Customers_2018_morethan1_loan) AND
     ls.lastAction IN ('Loan Redemption','Drop Loan')                AND
     YEAR(ls.OriginDate) >= 2018                                     AND
     ls.PrimaryLoan IS NOT NULL                                      AND
     ls.LoanCountry = 'Mex'           
    ) aux
    WHERE
     rank = 1
    ),

    --------------------
    -- Union of the y's for both types of customes
    y AS (
    SELECT
     *
    FROM
     y_1_loan
    UNION ALL
    SELECT
     *
    FROM
     y_morethan1_loan
    )

    ----------------
    -- Print the results to save as CSV
    SELECT * FROM y



    -------------------
    --   Validations --
    -------------------
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM y_1_loan --287,564
    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM y_morethan1_loan --93,209

    --SELECT COUNT(1), COUNT(DISTINCT Customer) FROM y --380,773 = 287,564+93,209



# Data Extraction

## Loan & Customer Features


```python
loan_cust=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/Loan_customer_features.csv")
```


```python
loan_cust.columns.tolist()
```




    ['Customer',
     'total_loans',
     'total_loan_redemption',
     'total_drop_loan',
     'total_Originalloan_loan_redemption',
     'total_Originalloan_drop_loan',
     'total_items',
     'avg_items_per_loan',
     'sd_items_per_loan',
     'max_items_per_loan',
     'total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'total_renewals',
     'avg_renewals',
     'max_renewals',
     'min_renewals',
     'total_RenewalDollars',
     'avg_RenewalDollars',
     'sd_RenewalDollars',
     'Gender',
     'City',
     'state',
     'birth_date',
     'zip']



## Categories Features


```python
catg=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/Category_Features.csv")
```


```python
catg.columns.tolist()
```




    ['Customer',
     'Bulks',
     'Cameras_Optics',
     'Earrings',
     'Electronics',
     'Firearms',
     'Household_Office',
     'Jewelry',
     'Musical_Gear',
     'Quantity_Based_Items',
     'Sports_Recreation',
     'Tools_Garden',
     'Transportation',
     'ind_Jewelry']



## MonthlyCharges Features


```python
mon_ch=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/MonthlyCharge_features.csv")
```


```python
mon_ch.columns.tolist()
```




    ['Customer',
     'total_MonthlyCharge',
     'avg_MonthlyCharge',
     'sd_MonthlyCharge',
     'max_MonthlyCharge',
     'min_MonthlyCharge']



## Store Table


```python
store_loan_data=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/store_per_loan_features.csv")
```


```python
store_loan_data.columns.tolist()
```




    ['Customer', 'PrimaryLoan', 'store', 'OriginDate', 'rank']



## Store Distribution - Mexico

Luis David (Category Manager) has a mexican Store Distribution in an Excel. This is the one we are going to use to know the state of each store, joining this table with the store number per loan.


```python
store_dist=pd.read_excel("/Users/darias/Documents/New_Customer_model_V2/Data/Distribución Nov FY19_2.xlsx", sheet = "Distribución")
```


```python
store_dist.rename(columns={'Store 1':'store'}, inplace=True)
```


```python
store_dist.columns.tolist()
```




    ['store',
     'Nombre',
     'Distrito',
     'Región',
     'Estado',
     'Municipio/Ciudad',
     'Apertura',
     'Tipo Tienda',
     'Distrito.1',
     'Región.1']



## Store distance


```python
distance=pd.read_csv("/Users/darias/Desktop/Tiendas_empeño_mexico/distancias_ezpawn_denue.csv",delimiter="|")
```


```python
distance.columns.tolist()
```




    ['num_tienda',
     'tienda_ezpawn',
     'tienda_denue',
     'direccion_denue',
     'latitud_ezpawn',
     'longitud_ezpawn',
     'latitud_denue',
     'longitud_denue',
     'dist',
     'cercania']



## Hour, day & Month


```python
hdm=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/hour_day_month_features.csv")
```


```python
hdm.columns.tolist()
```




    ['Customer',
     'early_morning',
     'morning',
     'meal',
     'afternoon',
     'night',
     'weekday',
     'friday',
     'weekend',
     'payday',
     'not_payday',
     'january',
     'february',
     'march',
     'april',
     'may',
     'june',
     'july',
     'august',
     'september',
     'october',
     'november',
     'december']



## Marginalization


```python
dtypes = {'codigo_postal':'str', 'grado_marginacion':'str'}
```


```python
marg=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/indice_marginacion_codigo_postal.txt",delimiter=",", dtype = dtypes)
```


```python
marg.rename(columns={'codigo_postal':'zip'}, inplace=True)
```


```python
marg.rename(columns={'grado_marginacion':'marginalization'}, inplace=True)
```


```python
marg=(
marg >>
    select(X.zip,
           X.marginalization)
)
```


```python
marg.columns.tolist()
```




    ['zip', 'marginalization']



## Y table


```python
y_data=pd.read_csv("/Users/darias/Documents/New_Customer_model_V2/Data/y_lastaction.csv")
```


```python
y_data.columns.tolist()
```




    ['Customer', 'lastAction']



# Store Features - Data Minning

## Competition near stores

Steps to obtain this feature:
 1. Leave the last store where each customer made a loan
 2. Calculate the numbre of stores of the competition near 500 m & 100 m to that store 
 3. Add a columns with the number of stores of the competition for each customer

### Last store for customer


```python
last_store=store_loan_data[store_loan_data['rank']==1][['Customer','store']]
```

### Number of competition near each store (500m)


```python
competitors=(
distance >>
    mask(X.dist <= 0.5) >>
    group_by(X.num_tienda) >>
    summarise(competitors_500 = n(X.tienda_denue))
)
```


```python
competitors.rename(columns={'num_tienda':'store'}, inplace=True)
```


```python
competitors.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>store</th>
      <th>competitors_500</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>85001</td>
      <td>2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>85002</td>
      <td>11</td>
    </tr>
  </tbody>
</table>
</div>



### Number of competition near each store (100m)


```python
competitors2=(
distance >>
    mask(X.dist <= 0.1) >>
    group_by(X.num_tienda) >>
    summarise(competitors_100 = n(X.tienda_denue))
)
```


```python
competitors2.rename(columns={'num_tienda':'store'}, inplace=True)
```


```python
competitors2.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>store</th>
      <th>competitors_100</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>85001</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>85002</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Number of competitors for last store

Joining the data of the last sotore each customer made a loan and the number of comptetiros near that store


```python
competitors_customer=(
last_store >>
    left_join(competitors,  by = 'store') >>
    left_join(competitors2, by = 'store')
)
```

Filling the NaN with 0, because a store with NaN comptetitors near, means that no has comptetitors


```python
competitors_customer=competitors_customer.fillna(0)
```


```python
competitors_customer.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer</th>
      <th>store</th>
      <th>competitors_500</th>
      <th>competitors_100</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>8590</td>
      <td>85132</td>
      <td>7.0</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>29408</td>
      <td>85127</td>
      <td>2.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>30451</td>
      <td>85179</td>
      <td>12.0</td>
      <td>4.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>41798</td>
      <td>85004</td>
      <td>2.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>59335</td>
      <td>85303</td>
      <td>15.0</td>
      <td>2.0</td>
    </tr>
  </tbody>
</table>
</div>



## Pct loans in State of residence

Create the feature of the number of loans in the state of residence of the customers

Keep only store and state from the store distribution


```python
store_dist=(
store_dist >>
    select(X.store, X.Estado)
)
```

Join with the store per loan


```python
store_loan_data=(
store_loan_data >>
    left_join(store_dist, by = 'store')
)
```


```python
store_loan_data.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer</th>
      <th>PrimaryLoan</th>
      <th>store</th>
      <th>OriginDate</th>
      <th>rank</th>
      <th>Estado</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>8590</td>
      <td>328682628</td>
      <td>85132</td>
      <td>2018-06-25 17:03:56 +0000</td>
      <td>1</td>
      <td>Aguascalientes</td>
    </tr>
    <tr>
      <th>1</th>
      <td>29408</td>
      <td>342734122</td>
      <td>85127</td>
      <td>2018-12-19 18:25:59 +0000</td>
      <td>1</td>
      <td>Guanajuato</td>
    </tr>
  </tbody>
</table>
</div>



Join with customer residence state

The table we create, is going to join with the customer state of residence, to know how many loans each customer did in their state of residence.


```python
store_loan_data=(
    store_loan_data >>
     left_join(loan_cust >> select(X.Customer, X.state), by = 'Customer')
)
```

Store and Residence state in the same format


```python
store_loan_data=(
store_loan_data >>
    mask(X.Estado != '') >>
    mutate(Estado = X.Estado.replace('Aguascalientes', 'Aguascalientes')) >>
    mutate(Estado = X.Estado.replace('Campeche', 'Campeche')) >>
    mutate(Estado = X.Estado.replace('Chiapas', 'Chiapas')) >>
    mutate(Estado = X.Estado.replace('Coahuila', 'Coahuila')) >>
    mutate(Estado = X.Estado.replace('D.F.', 'Distrito Federal')) >>
    mutate(Estado = X.Estado.replace('Edomex', 'Mexico')) >>
    mutate(Estado = X.Estado.replace('Guanajuato', 'Guanajuato')) >>
    mutate(Estado = X.Estado.replace('Guerrero', 'Guerrero')) >>
    mutate(Estado = X.Estado.replace('Hidalgo', 'Hidalgo')) >>
    mutate(Estado = X.Estado.replace('Jalisco', 'Jalisco')) >>
    mutate(Estado = X.Estado.replace('Michoacán', 'Michoacan De Ocampo')) >>
    mutate(Estado = X.Estado.replace('Morelos', 'Morelos')) >>
    mutate(Estado = X.Estado.replace('Nuevo León', 'Nuevo Leon')) >>
    mutate(Estado = X.Estado.replace('Oaxaca', 'Oaxaca')) >>
    mutate(Estado = X.Estado.replace('Puebla', 'Puebla')) >>
    mutate(Estado = X.Estado.replace('Querétaro', 'Queretaro')) >>
    mutate(Estado = X.Estado.replace('Quintana Roo', 'Quintana Roo')) >>
    mutate(Estado = X.Estado.replace('San Luis Potosí', 'San Luis Potosi')) >>
    mutate(Estado = X.Estado.replace('Sinaloa', 'Sinaloa')) >>
    mutate(Estado = X.Estado.replace('Tabasco', 'Tabasco')) >>
    mutate(Estado = X.Estado.replace('Tamaulipas', 'Tamaulipas')) >>
    mutate(Estado = X.Estado.replace('Tlaxcala', 'Tlaxcala')) >>
    mutate(Estado = X.Estado.replace('Veracruz', 'Veracruz'))
)
```


```python
store_loan_data=store_loan_data.dropna(subset=['Estado'])
```

Create the pct of loans in residence state


```python
pct_loans_residence_state=(
store_loan_data >>
    mutate(ind = if_else(X.Estado == X.state, 1, 0)) >>
    group_by(X.Customer) >>
    summarise(pct_loans_residence_state = X.ind.sum()/n(X.Customer))
)
```

Loan in state of residencia indicator


```python
pct_loans_residence_state=(
pct_loans_residence_state >>
    mutate(ind_loan_residence_state=if_else(X.pct_loans_residence_state > 0, 1, 0))
)
```

## Number of loans per state

Number of loans for each store state


```python
store_state_pct=(
store_loan_data >>
    group_by(X.Customer, X.Estado) >>
    summarize(n = n(X.Customer)) >>
    group_by(X.Customer) >>
    mutate(total = X.n.sum())
).pivot(index='Customer', columns='Estado', values='total')
```


```python
store_state_pct=store_state_pct.fillna(0)
```


```python
store_state_pct.columns=store_state_pct.columns.str.replace(' ','')
```


```python
store_state_pct.columns='num_store_'+store_state_pct.columns
```


```python
(
store_state_pct >>
    head(2)
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>num_store_Aguascalientes</th>
      <th>num_store_Campeche</th>
      <th>num_store_Chiapas</th>
      <th>num_store_Coahuila</th>
      <th>num_store_DistritoFederal</th>
      <th>num_store_Guanajuato</th>
      <th>num_store_Guerrero</th>
      <th>num_store_Hidalgo</th>
      <th>num_store_Jalisco</th>
      <th>num_store_Mexico</th>
      <th>...</th>
      <th>num_store_Oaxaca</th>
      <th>num_store_Puebla</th>
      <th>num_store_Queretaro</th>
      <th>num_store_QuintanaRoo</th>
      <th>num_store_SanLuisPotosi</th>
      <th>num_store_Sinaloa</th>
      <th>num_store_Tabasco</th>
      <th>num_store_Tamaulipas</th>
      <th>num_store_Tlaxcala</th>
      <th>num_store_Veracruz</th>
    </tr>
    <tr>
      <th>Customer</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>8590</th>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>29408</th>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>2 rows × 23 columns</p>
</div>



## Join Pct state of residence, competitors & loans for state


```python
store_features=(
competitors_customer >>
    left_join(pct_loans_residence_state, by = 'Customer') >>
    left_join(store_state_pct, by = 'Customer')
).drop("store", axis = 1)
```


```python
store_features.columns.tolist()
```




    ['Customer',
     'competitors_500',
     'competitors_100',
     'pct_loans_residence_state',
     'ind_loan_residence_state',
     'num_store_Aguascalientes',
     'num_store_Campeche',
     'num_store_Chiapas',
     'num_store_Coahuila',
     'num_store_DistritoFederal',
     'num_store_Guanajuato',
     'num_store_Guerrero',
     'num_store_Hidalgo',
     'num_store_Jalisco',
     'num_store_Mexico',
     'num_store_MichoacanDeOcampo',
     'num_store_Morelos',
     'num_store_NuevoLeon',
     'num_store_Oaxaca',
     'num_store_Puebla',
     'num_store_Queretaro',
     'num_store_QuintanaRoo',
     'num_store_SanLuisPotosi',
     'num_store_Sinaloa',
     'num_store_Tabasco',
     'num_store_Tamaulipas',
     'num_store_Tlaxcala',
     'num_store_Veracruz']



# Data Cleaning

## Loan Summary & Customer Features

Just Customers with correct zip codes:
 - 5 digit & just numbers
 - Customers Zip's that are in the zip table


```python
# Just numbers in zip
loan_cust=loan_cust[loan_cust['zip'].astype(str).str.isdigit()]
```


```python
# Zip's with 5 characters
loan_cust=loan_cust[loan_cust['zip'].apply(len)==5]
```


```python
# Zip's in Zip table
loan_cust=loan_cust.loc[loan_cust['zip'].isin(marg['zip'].astype('str').tolist())]
```

## Categories Features


```python
catg=catg.fillna(0)
```

## MonthlyCharge


```python
mon_ch=mon_ch.fillna(0)
```

## Y


```python
y_data=(
y_data >>
    mutate(y_red=if_else(X.lastAction=='Loan Redemption', 1, 0))
)
```

# Data Engineering

## Join all features & y


```python
datas=(loan_cust >>
  left_join(catg     ,      by = 'Customer') >>
  left_join(store_features, by = 'Customer') >>
  left_join(mon_ch,         by = 'Customer') >>
  left_join(hdm   ,         by = 'Customer') >>
  left_join(marg   ,        by = 'zip')      >>
  left_join(y_data   ,      by = 'Customer')
)
```


```python
datas=datas.dropna(subset=['lastAction'])
```


```python
datas=datas.dropna(subset=['state'])
```


```python
datas=datas.dropna(subset=['num_store_Aguascalientes'])
```

## Create Age


```python
datas['birth_date']=pd.to_datetime(datas['birth_date'])
```


```python
datas['age']=(dt.datetime.now()-datas['birth_date']).dt.days
```

Delete age >= 80


```python
datas=(
datas >>
    mutate(age = (X.age/365).round()) >>
    mask(X.age<=80)
)
```

## Classify in North, center & south the residence state 

Only customers with a mexican residence state


```python
mex_states=[
'Veracruz',
'Distrito Federal',
'Mexico',
'Tabasco',
'Jalisco',
'Campeche',
'Puebla',
'Guanajuato',
'Sinaloa',
'Hidalgo',
'Chiapas',
'Michoacan De Ocampo',
'Tamaulipas',
'Guerrero',
'Queretaro',
'Tlaxcala',
'Quintana Roo',
'Oaxaca',
'Nuevo Leon',
'Aguascalientes',
'Morelos',
'Coahuila',
'San Luis Potosi',
'Baja California',
'Yucatan',
'Nayarit',
'Durango',
'Chihuahua',
'Sonora',
'Zacatecas',
'Baja California Sur',
'Colima'
]
```


```python
datas=(
datas >>
    mask(X.state.isin(mex_states))
)
```

Define North, center & south states


```python
north=['Sinaloa', 'Tamaulipas', 'Nuevo Leon', 'Coahuila', 'San Luis Potosi',
       'Baja California', 'Nayarit', 'Durango', 'Chihuahua', 'Sonora',
       'Zacatecas', 'Baja California Sur', 'Colima']

center=['Veracruz', 'Distrito Federal', 'Mexico', 'Jalisco',
        'Puebla', 'Guanajuato', 'Hidalgo', 'Michoacan De Ocampo',
        'Queretaro', 'Aguascalientes', 'Morelos']

south=['Tabasco', 'Campeche', 'Chiapas', 'Guerrero', 'Tlaxcala', 'Quintana Roo', 'Oaxaca', 'Yucatan']
```

Create geographic indicator as a feature


```python
datas=(
datas >>
    mutate(ind_geo = if_else(X.state.isin(north), 'north',
                            if_else(X.state.isin(center), 'center',
                                   if_else(X.state.isin(south), 'south', 'NI'))))
)
```


```python
datas.columns.tolist()
```




    ['Customer',
     'total_loans',
     'total_loan_redemption',
     'total_drop_loan',
     'total_Originalloan_loan_redemption',
     'total_Originalloan_drop_loan',
     'total_items',
     'avg_items_per_loan',
     'sd_items_per_loan',
     'max_items_per_loan',
     'total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'total_renewals',
     'avg_renewals',
     'max_renewals',
     'min_renewals',
     'total_RenewalDollars',
     'avg_RenewalDollars',
     'sd_RenewalDollars',
     'Gender',
     'City',
     'state',
     'birth_date',
     'zip',
     'Bulks',
     'Cameras_Optics',
     'Earrings',
     'Electronics',
     'Firearms',
     'Household_Office',
     'Jewelry',
     'Musical_Gear',
     'Quantity_Based_Items',
     'Sports_Recreation',
     'Tools_Garden',
     'Transportation',
     'ind_Jewelry',
     'competitors_500',
     'competitors_100',
     'pct_loans_residence_state',
     'ind_loan_residence_state',
     'num_store_Aguascalientes',
     'num_store_Campeche',
     'num_store_Chiapas',
     'num_store_Coahuila',
     'num_store_DistritoFederal',
     'num_store_Guanajuato',
     'num_store_Guerrero',
     'num_store_Hidalgo',
     'num_store_Jalisco',
     'num_store_Mexico',
     'num_store_MichoacanDeOcampo',
     'num_store_Morelos',
     'num_store_NuevoLeon',
     'num_store_Oaxaca',
     'num_store_Puebla',
     'num_store_Queretaro',
     'num_store_QuintanaRoo',
     'num_store_SanLuisPotosi',
     'num_store_Sinaloa',
     'num_store_Tabasco',
     'num_store_Tamaulipas',
     'num_store_Tlaxcala',
     'num_store_Veracruz',
     'total_MonthlyCharge',
     'avg_MonthlyCharge',
     'sd_MonthlyCharge',
     'max_MonthlyCharge',
     'min_MonthlyCharge',
     'early_morning',
     'morning',
     'meal',
     'afternoon',
     'night',
     'weekday',
     'friday',
     'weekend',
     'payday',
     'not_payday',
     'january',
     'february',
     'march',
     'april',
     'may',
     'june',
     'july',
     'august',
     'september',
     'october',
     'november',
     'december',
     'marginalization',
     'lastAction',
     'y_red',
     'age',
     'ind_geo']



## Just GM


```python
datas=(
datas >>
    mask(X.ind_Jewelry==0)
)
```

# Descriptive Analysis

Some descriptives...

## Redeem & Drop

### Totals


```python
(datas >>
  group_by(X.lastAction) >>
  summarize(n = n(X.lastAction)) >>
  ggplot() +
  geom_bar(aes(x = 'lastAction', y = 'n'), stat = 'identity', fill = 'darkblue') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'n', label = 'n'), va='bottom',format_string='{:,}') +
  scale_y_continuous(labels = comma_format()) +
  ggtitle('Drop vs No Drop Counts')
)
```


![png](output_153_0.png)





    <ggplot: (7551960752)>



### Distribution


```python
(
datas >>
    group_by(X.lastAction) >>
    summarise(pct = n(X.lastAction)/len(datas)) >>
    ggplot() +
    geom_bar(aes(x = 'lastAction', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    geom_text(aes(x = 'lastAction', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = '{}%') +
    scale_y_continuous(labels = percent_format()) +
    ggtitle('Pct Drop & Redeem')
)
```


![png](output_155_0.png)





    <ggplot: (7551924705)>



## Total Loans

### Distribution


```python
(
datas >>
    group_by(X.total_loans) >>
    summarise(pct = n(X.total_loans)/len(datas)) >>
    ggplot() +
    geom_bar(aes(x = 'total_loans', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    geom_text(aes(x = 'total_loans', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = '{}%') +
    scale_y_continuous(labels = percent_format()) +
    ggtitle('Pct Total Loans')
)
```


![png](output_158_0.png)





    <ggplot: (-9223372029302873575)>



### Drop & Redeem Pct


```python
(
datas >>
  group_by(X.total_loans, X.lastAction) >>
  summarise(n = n(X.total_loans)) >>
  group_by(X.total_loans) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'total_loans', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop & Redeem Pct - Total Loans') +
  geom_text(aes(x = 'total_loans', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_160_0.png)





    <ggplot: (-9223372029303410407)>



### Average Original Loan per total loans


```python
(
datas >>
    group_by(X.total_loans) >>
    summarise(avg_OriginalLoan = median(X.avg_originalloan_per_loan)) >>
    ggplot() +
    geom_bar(aes(x = 'total_loans', y = 'avg_OriginalLoan'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    ggtitle('Avg Original Loan - Total Loans') +
    geom_text(aes(x = 'total_loans', y = 'avg_OriginalLoan', label = 'avg_OriginalLoan'), va='bottom', format_string='${}')
)
```


![png](output_162_0.png)





    <ggplot: (-9223372029303447152)>



## Age

### Distribution


```python
(datas >>
  ggplot() +
  geom_histogram(aes('age'), fill = 'darkblue') + 
  ggtitle('Age Distribution') +
  theme_bw() +
  scale_x_continuous(limits = [17,80]) +
  geom_vline(xintercept = median(datas['age'])) +
  annotate(geom   = 'text',
           x      = median(datas['age']),
           y      = 12000,
           label  = 'Median = '+str(median(datas['age']))+' years',
           ha     = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 112'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_165_1.png)





    <ggplot: (-9223372029303414682)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.age)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Age - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_167_0.png)





    <ggplot: (-9223372029303382342)>



### Boxplot - Drop vs Redeem


```python
(
datas >>
    ggplot() +
    geom_boxplot(aes(x = 'lastAction', y = 'age')) +
    theme_bw() +
    ggtitle('Boxplot Age - Drop vs Redeem')
)
```


![png](output_169_0.png)





    <ggplot: (-9223372029302811346)>



## Gender

### Distribution


```python
(
datas >>
  group_by(X.Gender) >>
  summarise(n = n(X.Gender)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'Gender', y = 'n'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Gender Pct') +
  geom_text(aes(x='Gender',y='n',label='round(n*100,2)'),va='bottom',size=8,format_string='{}%') +
  xlab('') +
  ylab('') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_172_0.png)





    <ggplot: (7551388606)>




```python
(
loan_cust >>
    group_by(X.Gender) >>
    summarise(n = n(X.Gender))
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>n</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Female</td>
      <td>107306</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Male</td>
      <td>212761</td>
    </tr>
  </tbody>
</table>
</div>



## Redeem PCT


```python
(
datas >>
  group_by(X.Gender, X.lastAction) >>
  summarise(n = n(X.Gender)) >>
  group_by(X.Gender) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'Gender', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop & Redeem Pct - Gender') +
  geom_text(aes(x = 'Gender', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_175_0.png)





    <ggplot: (297768789)>



## Geographical Indicator

### Distribution


```python
order_ind_geo=(
datas >>
  group_by(X.ind_geo) >>
  summarise(n = n(X.ind_geo)/len(datas))
).sort_values(by=['n'])['ind_geo'].tolist()

(
datas >>
  group_by(X.ind_geo) >>
  summarise(n = n(X.ind_geo)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'ind_geo', y = 'n'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Geographical Indicator Pct') +
  geom_text(aes(x = 'ind_geo', y = 'n', label = 'round(n*100,2)'), va='bottom', format_string='{}%') +
  xlab('') +
  ylab('') +
  scale_x_discrete(limits = order_ind_geo) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_178_0.png)





    <ggplot: (-9223372036557385805)>



### Redeem Pct


```python
(
datas >>
  group_by(X.ind_geo, X.lastAction) >>
  summarise(n = n(X.ind_geo)) >>
  group_by(X.ind_geo) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'ind_geo', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop Pct - Geographical Indicator') +
  geom_text(aes(x = 'ind_geo', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_180_0.png)





    <ggplot: (-9223372036557385763)>



## Average Original Loan

### Distribution


```python
(
datas >>
  ggplot() +
  geom_histogram(aes('avg_originalloan_per_loan'), fill = 'darkblue') +
  ggtitle('Average Original Loan - Distribution') +
  theme_bw() +
  scale_x_continuous(limits =[0,2000]) +
  geom_vline(xintercept = median(datas['avg_originalloan_per_loan'])) +
  annotate(geom  = 'text',
            x     = median(datas['avg_originalloan_per_loan']),
            y     = 18000,
            label = 'Median = $'+str(round(median(datas['avg_originalloan_per_loan']),0)),
            ha    = 'left')
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/stats/stat_bin.py:93: UserWarning: 'stat_bin()' using 'bins = 521'. Pick better value with 'binwidth'.
      warn(msg.format(params['bins']))
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_bin : Removed 7044 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_183_1.png)





    <ggplot: (-9223372029300416459)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.avg_originalloan_per_loan)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Average Original Loan - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_185_0.png)





    <ggplot: (-9223372029300630833)>



## Renewals

### Distribution


```python
(
datas >>
    group_by(X.total_renewals) >>
    summarise(pct = n(X.total_renewals)/len(datas)) >>
    ggplot() +
    geom_bar(aes(x = 'total_renewals', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    geom_text(aes(x = 'total_renewals', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = '{}%', size = 10) +
    scale_y_continuous(labels = percent_format()) +
    ggtitle('Pct Total Renewals') +
    scale_x_continuous(limits = [-0.5,6])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:32: RuntimeWarning: invalid value encountered in reduce
      return umr_minimum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/numpy/core/_methods.py:28: RuntimeWarning: invalid value encountered in reduce
      return umr_maximum(a, axis, None, out, keepdims, initial)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:430: UserWarning: position_stack : Removed 17 rows containing missing values.
      data = self.position.setup_data(self.data, params)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_bar : Removed 1 rows containing missing values.
      self.data = self.geom.handle_na(self.data)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_text : Removed 17 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_188_1.png)





    <ggplot: (-9223372029302856153)>



### Mean - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = mean(X.total_renewals)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Total Renewals - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_190_0.png)





    <ggplot: (-9223372029300639267)>



## Average Monthly Charges

### Distributions


```python
(datas >>
  ggplot() +
  geom_histogram(aes('avg_MonthlyCharge'), fill = 'darkblue', bins = 80) + 
  ggtitle('Avg Monthly Charge Distribution') +
  theme_bw() +
  geom_vline(xintercept = median(datas['avg_MonthlyCharge'])) +
  annotate(geom   = 'text',
           x      = median(datas['avg_MonthlyCharge']),
           y      = 23000,
           label  = 'Median = $'+str(median(datas['avg_MonthlyCharge'])),
           ha     = 'left') +
 scale_x_continuous(limits = [-0.5,1000])
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:360: UserWarning: stat_bin : Removed 1001 rows containing non-finite values.
      data = self.stat.compute_layer(data, params, layout)
    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:449: UserWarning: geom_histogram : Removed 2 rows containing missing values.
      self.data = self.geom.handle_na(self.data)



![png](output_193_1.png)





    <ggplot: (297379848)>



### Median - Drop vs Redeem


```python
(
datas >>
  group_by(X.lastAction) >>
  summarise(med = median(X.avg_MonthlyCharge)) >>
  ggplot() +
  geom_col(aes(x = 'lastAction', y = 'med'), fill = 'darkblue') +
  ggtitle('Median of Average Monthly Charge - Drop & No Drop') +
  theme_bw() +
  geom_text(aes(x = 'lastAction', y = 'med', label = 'round(med)'), va='bottom')
)
```


![png](output_195_0.png)





    <ggplot: (-9223372029300415379)>



## Residence State


```python
order_state=(
datas >>
  group_by(X.state) >>
  summarise(pct = n(X.state)/len(datas))
).sort_values(by=['pct'])['state'].tolist()

(
datas >>
  group_by(X.state) >>
  summarise(pct = n(X.state)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'state', y = 'pct'), fill = 'darkblue') +
  ggtitle('State Distribution') +
  theme_bw() +
  geom_text(aes(x = 'state', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%', angle = 90)+
  scale_x_discrete(limits=order_state) +
  theme(axis_text_x = element_text(angle = 90)) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_197_0.png)





    <ggplot: (7554371343)>



### Pct of Redeem


```python
order_states_red=(
datas >>
  group_by(X.state, X.y_red) >>
  summarise(n = n(X.state)) >>
  group_by(X.state) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  mask(X.y_red == 1.0)
).sort_values(by=['pct'],ascending=True)['state'].tolist()

(
datas >>
  group_by(X.state, X.y_red) >>
  summarise(n = n(X.state)) >>
  group_by(X.state) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total,
         pct_total = X.n/len(datas)) >>
  mask(X.y_red == 1.0) >>
  ggplot() +
  geom_col(aes(x = 'state', y = 'pct', fill = 'pct_total')) +
  ggtitle('Pct of Drop per state') +
  theme_bw() +
  geom_text(aes(x = 'state', y = 'pct', label = 'round(pct*100,2)'), va='bottom', angle = 90, format_string='{}%',size=8)+
  theme(axis_text_x = element_text(angle = 90)) +
  scale_x_discrete(limits=order_states_red) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_199_0.png)





    <ggplot: (7554371350)>



## Loans in State of Residence

### Distribution


```python
(
datas >>
    mutate(ind = if_else(X.ind_loan_residence_state==1, 'State of Residence', 'Other State')) >>
    group_by(X.ind) >>
    summarise(pct = n(X.ind)/len(datas)) >>
    ggplot() +
    geom_bar(aes(x = 'ind', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    geom_text(aes(x = 'ind', y = 'pct', label = 'round(pct*100,2)'),va='bottom',format_string='{}%') +
    scale_y_continuous(labels = percent_format()) +
    ggtitle('Residence State Loan Indicator - Distribution')
)
```


![png](output_202_0.png)





    <ggplot: (7553680192)>



### Redeem Pct


```python
(
datas >>
  mutate(ind = if_else(X.ind_loan_residence_state==1, 'State of Residence', 'Other State')) >>
  group_by(X.ind, X.lastAction) >>
  summarise(n = n(X.ind)) >>
  group_by(X.ind) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'ind', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Redeem Pct - Residence State Loan Indicator') +
  geom_text(aes(x = 'ind', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%') +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_204_0.png)





    <ggplot: (7553681847)>



## Number of stores of the competition

### Distribution


```python
(datas >>
    group_by(X.competitors_500) >>
    summarise(pct = n(X.competitors_500)/len(datas)) >>
    ggplot() +
    geom_bar(aes(x = 'competitors_500', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    geom_text(aes(x = 'competitors_500', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = ' {}%', size = 10, angle = 90)+
    scale_y_continuous(labels = percent_format()) +
    ggtitle('# Stores Competition 500 m - Distribution') +
    geom_vline(xintercept = median(datas['competitors_500'])) +
  annotate(geom   = 'text',
           x      = median(datas['competitors_500']),
           y      = 0.14,
           label  = 'Median = '+str(median(datas['competitors_500']))+' stores',
           ha     = 'left')
    )
```


![png](output_207_0.png)





    <ggplot: (-9223372029303481221)>



## Weekend, Friday & Weekday

### Distribution


```python
day_desc=pd.DataFrame(index=['weekday', 'friday', 'weekend'],
             columns=['desc','count','pct', 'pct_redeem'])
```


```python
day_desc.loc['weekday','desc']='weekday'
day_desc.loc['friday','desc']='friday'
day_desc.loc['weekend','desc']='weekend'

day_desc.loc['weekday','count']=datas['weekday'].sum()
day_desc.loc['friday','count']=datas['friday'].sum()
day_desc.loc['weekend','count']=datas['weekend'].sum()

day_desc.loc['weekday','pct']=day_desc.loc['weekday','count']/len(datas)
day_desc.loc['friday','pct']=day_desc.loc['friday','count']/len(datas)
day_desc.loc['weekend','pct']=day_desc.loc['weekend','count']/len(datas)

day_desc['pct']=day_desc['pct'].astype('float')

(day_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Week Distribution') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_211_0.png)





    <ggplot: (297301540)>



### Pct Redeem


```python
day_desc.loc['weekday','pct_redeem']=datas['weekday'][datas['y_red']==1].sum()/day_desc.loc['weekday','count']
day_desc.loc['friday','pct_redeem']=datas['friday'][datas['y_red']==1].sum()/day_desc.loc['friday','count']
day_desc.loc['weekend','pct_redeem']=datas['weekend'][datas['y_red']==1].sum()/day_desc.loc['weekend','count']

day_desc['pct_redeem']=day_desc['pct_redeem'].astype('float')

(day_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct_redeem'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Week - Pct Redeem') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct_redeem', label = 'round(pct_redeem*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_213_0.png)





    <ggplot: (297316381)>



## Hour Day


```python
hour_desc=pd.DataFrame(index=['early_morning', 'morning', 'meal', 'afternoon', 'night'],
             columns=['desc','count','pct', 'pct_redeem'])
```


```python
hour_desc.loc['early_morning','desc']='early_morning'
hour_desc.loc['morning','desc']='morning'
hour_desc.loc['meal','desc']='meal'
hour_desc.loc['afternoon','desc']='afternoon'
hour_desc.loc['night','desc']='night'

hour_desc.loc['early_morning','count']=datas['early_morning'].sum()
hour_desc.loc['morning','count']=datas['morning'].sum()
hour_desc.loc['meal','count']=datas['meal'].sum()
hour_desc.loc['afternoon','count']=datas['afternoon'].sum()
hour_desc.loc['night','count']=datas['night'].sum()

hour_desc.loc['early_morning','pct']=hour_desc.loc['early_morning','count']/len(datas)
hour_desc.loc['morning','pct']=hour_desc.loc['morning','count']/len(datas)
hour_desc.loc['meal','pct']=hour_desc.loc['meal','count']/len(datas)
hour_desc.loc['afternoon','pct']=hour_desc.loc['afternoon','count']/len(datas)
hour_desc.loc['night','pct']=hour_desc.loc['night','count']/len(datas)

hour_desc['pct']=hour_desc['pct'].astype('float')

(hour_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Hour Distribution') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_216_0.png)





    <ggplot: (7551306977)>



### Pct Redeem


```python
hour_desc.loc['early_morning','pct_redeem']=datas['early_morning'][datas['y_red']==1].sum()/hour_desc.loc['early_morning','count']
hour_desc.loc['morning','pct_redeem']=datas['morning'][datas['y_red']==1].sum()/hour_desc.loc['morning','count']
hour_desc.loc['meal','pct_redeem']=datas['meal'][datas['y_red']==1].sum()/hour_desc.loc['meal','count']
hour_desc.loc['afternoon','pct_redeem']=datas['afternoon'][datas['y_red']==1].sum()/hour_desc.loc['afternoon','count']
hour_desc.loc['night','pct_redeem']=datas['night'][datas['y_red']==1].sum()/hour_desc.loc['night','count']

hour_desc['pct_redeem']=hour_desc['pct_redeem'].astype('float')


(hour_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct_redeem'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Hour - Pct Redeem') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct_redeem', label = 'round(pct_redeem*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_218_0.png)





    <ggplot: (7553580029)>



## Payday

### Distribution


```python
payday_desc=pd.DataFrame(index=['payday', 'not_payday'],
                         columns=['desc','count','pct', 'pct_redeem'])
```


```python
payday_desc.loc['payday','desc']='payday'
payday_desc.loc['not_payday','desc']='not_payday'

payday_desc.loc['payday','count']=datas['payday'].sum()
payday_desc.loc['not_payday','count']=datas['not_payday'].sum()

payday_desc.loc['payday','pct']=payday_desc.loc['payday','count']/len(datas)
payday_desc.loc['not_payday','pct']=payday_desc.loc['not_payday','count']/len(datas)

payday_desc['pct']=payday_desc['pct'].astype('float')

(payday_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Payday Distribution') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct', label = 'round(pct*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_222_0.png)





    <ggplot: (-9223372029303435541)>



### Redeem Percent


```python
payday_desc.loc['payday','pct_redeem']=datas['payday'][datas['y_red']==1].sum()/payday_desc.loc['payday','count']
payday_desc.loc['not_payday','pct_redeem']=datas['not_payday'][datas['y_red']==1].sum()/payday_desc.loc['not_payday','count']

payday_desc['pct_redeem']=payday_desc['pct_redeem'].astype('float')


(payday_desc >>
 ggplot() +
 geom_bar(aes(x = 'desc', y = 'pct_redeem'), stat = 'identity', fill = 'darkblue') +
 scale_y_continuous(labels = percent_format()) +
 ggtitle('Payday - Pct Redeem') +
 theme_bw() +
 geom_text(aes(x = 'desc', y = 'pct_redeem', label = 'round(pct_redeem*100,2)'), va = 'bottom', format_string = ' {}%', size = 10)
)
```


![png](output_224_0.png)





    <ggplot: (297327238)>



## Marginalization

### Distribution


```python
order_marg=(
datas >>
  group_by(X.marginalization) >>
  summarise(n = n(X.marginalization)/len(datas))
).sort_values(by=['n'])['marginalization'].tolist()

(
datas >>
  group_by(X.marginalization) >>
  summarise(n = n(X.marginalization)/len(datas)) >>
  ggplot() +
  geom_col(aes(x = 'marginalization', y = 'n'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Marginalization Pct') +
  geom_text(aes(x = 'marginalization', y = 'n', label = 'round(n*100,2)'), va='bottom', format_string='{}%') +
  xlab('') +
  ylab('') +
  scale_x_discrete(limits = order_marg) +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_227_0.png)





    <ggplot: (297298766)>



### Redeem Pct


```python
(
datas >>
  group_by(X.marginalization, X.lastAction) >>
  summarise(n = n(X.marginalization)) >>
  group_by(X.marginalization) >>
  mutate(total = X.n.sum()) >>
  mutate(pct = X.n/X.total) >>
  ggplot() +
  geom_col(aes(x = 'marginalization', y = 'pct'), fill = 'darkblue') +
  theme_bw() +
  ggtitle('Drop Pct - Marginalization') +
  geom_text(aes(x = 'marginalization', y = 'pct', label = 'round(pct*100,2)'), va='bottom', format_string='{}%', size = 8) +
  facet_wrap('~lastAction') +
  scale_y_continuous(labels = percent_format())
)
```


![png](output_229_0.png)





    <ggplot: (-9223372029098131645)>



# One Hot Encoding

Dummies for the categorical features:
 - State of Residence (state)
 - Geographical Indicator (indgeo)
 - Total Loans


```python
state_dummies=pd.get_dummies(datas['state'])
ind_geo_dummies=pd.get_dummies(datas['ind_geo'])
total_loans_dummies=pd.get_dummies(datas['total_loans'])
gender_dummies=pd.get_dummies(datas['Gender'])
marg_dummies=pd.get_dummies(datas['marginalization'])
```

Fill NaN with 0


```python
state_dummies=state_dummies.fillna(0)
ind_geo_dummies=ind_geo_dummies.fillna(0)
total_loans_dummies=total_loans_dummies.fillna(0)
gender_dummies=gender_dummies.fillna(0)
marg_dummies=marg_dummies.fillna(0)
```


```python
total_loans_dummies.columns='total_loans_'+total_loans_dummies.columns.astype('str')
```

Join the dummies to the complete data


```python
datas=pd.concat([datas,state_dummies,ind_geo_dummies,total_loans_dummies,gender_dummies,marg_dummies],sort=False,axis=1)
```

Delete dummies df


```python
del(state_dummies)
del(ind_geo_dummies)
del(total_loans_dummies)
del(gender_dummies)
del(marg_dummies)
```

# Clean Memory

Delete all the df not going to use


```python
del(loan_cust)
del(catg)
del(mon_ch)
del(store_loan_data)
del(store_dist)
del(distance)
del(y_data)
del(last_store)
del(competitors)
del(competitors_customer)
del(pct_loans_residence_state)
del(store_state_pct)
del(store_features)
del(hdm)
```

Save data into a pickle


```python
datas.to_pickle('data_new_customer_model.pkl')
```


```python
#datas=pd.read_pickle('data_new_customer_model.pkl')
```

# Sample Data

Create a 50% sample data to fit the model faster


```python
datas=datas.sample(frac=0.5, replace=False, random_state=1)
```

Saving sample data


```python
datas.to_pickle('data_new_customer_model_sample.pkl')
```


```python
#datas=pd.read_pickle('data_new_customer_model_sample.pkl')
```

# Redeem Model

Fit a **LightGBM** with a balanced data in train, optimizing hiperparameters in a unbalanced set _(test)_, testing the efectiveness in a unbalanced set _(validation)_

## X & Y

Define the name of the features to use to fit the model


```python
x=[feat for feat in datas.columns if feat not in ['Customer', 'Gender', 'City','state','birth_date', 'total_loans', 'marginalization',
                                                  'lastAction', 'y_red', 'ind_geo', 'per', 'zip', 'store', 'ind_Jewelry', 'Jewelry']]
y='y_red'
```


```python
x
```




    ['total_loan_redemption',
     'total_drop_loan',
     'total_Originalloan_loan_redemption',
     'total_Originalloan_drop_loan',
     'total_items',
     'avg_items_per_loan',
     'sd_items_per_loan',
     'max_items_per_loan',
     'total_originalloan',
     'avg_originalloan_per_loan',
     'sd_originalloan_per_loan',
     'total_renewals',
     'avg_renewals',
     'max_renewals',
     'min_renewals',
     'total_RenewalDollars',
     'avg_RenewalDollars',
     'sd_RenewalDollars',
     'Bulks',
     'Cameras_Optics',
     'Earrings',
     'Electronics',
     'Firearms',
     'Household_Office',
     'Musical_Gear',
     'Quantity_Based_Items',
     'Sports_Recreation',
     'Tools_Garden',
     'Transportation',
     'competitors_500',
     'competitors_100',
     'pct_loans_residence_state',
     'ind_loan_residence_state',
     'num_store_Aguascalientes',
     'num_store_Campeche',
     'num_store_Chiapas',
     'num_store_Coahuila',
     'num_store_DistritoFederal',
     'num_store_Guanajuato',
     'num_store_Guerrero',
     'num_store_Hidalgo',
     'num_store_Jalisco',
     'num_store_Mexico',
     'num_store_MichoacanDeOcampo',
     'num_store_Morelos',
     'num_store_NuevoLeon',
     'num_store_Oaxaca',
     'num_store_Puebla',
     'num_store_Queretaro',
     'num_store_QuintanaRoo',
     'num_store_SanLuisPotosi',
     'num_store_Sinaloa',
     'num_store_Tabasco',
     'num_store_Tamaulipas',
     'num_store_Tlaxcala',
     'num_store_Veracruz',
     'total_MonthlyCharge',
     'avg_MonthlyCharge',
     'sd_MonthlyCharge',
     'max_MonthlyCharge',
     'min_MonthlyCharge',
     'early_morning',
     'morning',
     'meal',
     'afternoon',
     'night',
     'weekday',
     'friday',
     'weekend',
     'payday',
     'not_payday',
     'january',
     'february',
     'march',
     'april',
     'may',
     'june',
     'july',
     'august',
     'september',
     'october',
     'november',
     'december',
     'age',
     'Aguascalientes',
     'Baja California',
     'Baja California Sur',
     'Campeche',
     'Chiapas',
     'Chihuahua',
     'Coahuila',
     'Colima',
     'Distrito Federal',
     'Durango',
     'Guanajuato',
     'Guerrero',
     'Hidalgo',
     'Jalisco',
     'Mexico',
     'Michoacan De Ocampo',
     'Morelos',
     'Nayarit',
     'Nuevo Leon',
     'Oaxaca',
     'Puebla',
     'Queretaro',
     'Quintana Roo',
     'San Luis Potosi',
     'Sinaloa',
     'Sonora',
     'Tabasco',
     'Tamaulipas',
     'Tlaxcala',
     'Veracruz',
     'Yucatan',
     'Zacatecas',
     'center',
     'north',
     'south',
     'total_loans_0',
     'total_loans_1',
     'total_loans_2',
     'Female',
     'Male',
     'Alto',
     'Bajo',
     'Medio',
     'Muy alto',
     'Muy bajo']



## Split

Split the data into:
 - Train (balanced)
 - Test (unbalanced)
 - Valid (unbalanced)


```python
X=datas[x]
Y=datas[y]
```

Create Train and validation


```python
Xt,Xv,yt,yv = train_test_split(X,Y,train_size=0.8, random_state = 10)
```

Split train, to balance one part and leave another unbalancet for test


```python
Xtrain, X_test, ytrain, ytest = train_test_split(Xt, yt, train_size = 0.8, random_state = 20)
```

### Undersampling Train


```python
# Separate majority and minority classes

# Majority class
x_data_majority = Xtrain.loc[ytrain == 0]
y_data_majority = ytrain.loc[ytrain == 0]

# Minority class
x_data_minority = Xtrain.loc[ytrain == 1]
y_data_minority = ytrain.loc[ytrain == 1]

# Downsample majority class
y_df_major_downsampled = sklearn.utils.resample(y_data_majority,replace=False,    # sample without replacement
                                n_samples=ytrain.value_counts()[1],  # to match minority class
                                random_state=10) # reproducible results

x_df_major_downsampled = sklearn.utils.resample(x_data_majority, replace=False,    # sample without replacement
                                n_samples=ytrain.value_counts()[1],  # to match minority class
                                random_state=10) # reproducible results

# Combine minority class with downsampled majority class
y_data_downsampled = pd.concat([y_df_major_downsampled, y_data_minority])
X_data_downsampled = pd.concat([x_df_major_downsampled, x_data_minority])

# Display new class counts
y_data_downsampled.value_counts()
```




    1    40108
    0    40108
    Name: y_red, dtype: int64



Now we have:
 - Train balanced _(ydatadownsampled, Xdatadownsampled)_
 - Test unbalanced _(Xtest, ytest)_
 - Valid unbalanced _(Xv, yv)_

## Hiperparameters

Grid of hiperparameters


```python
#0.06-9.0-10.0-600.0
p_learning_rate = np.array([0.05,0.06,0.07])
p_max_depth = np.array([3,9,15]) 
p_num_leaves = np.array([2,10,20])
p_num_iterations = np.array([500,600,700])
```

## Fit the model

Train set to _lgb dataset_


```python
d_train = lgb.Dataset(X_data_downsampled, label=y_data_downsampled)
```

AUC in test Matrix


```python
hp_perf_mat = np.zeros((p_learning_rate.shape[0],
                        p_max_depth.shape[0],
                        p_num_leaves.shape[0],
                        p_num_iterations.shape[0]))
i=1
```


```python
%%time
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                params = {}
                params['learning_rate'] = p_learning_rate[i]
                params['boosting_type'] = 'gbdt'
                params['objective'] = 'binary'
                params['metric'] = 'binary_logloss'
                params['sub_feature'] = 0.75 #0.75
                params['num_leaves'] = p_num_leaves[k]
                params['min_data'] = 200 #200
                params['max_depth'] = p_max_depth[j]
                params['num_iterations'] = p_num_iterations[l]
                params['num_threads'] = 5
                params['tree_learner'] = 'data'
                params['seed'] = 52
                params['lambda_l1'] = 0.5
                lgbm = lgb.train(params, d_train)
                # Evaluate AUC in TEST
                auc_i = roc_auc_score(ytest,lgbm.predict(X_test))
                # Add the AUC to the AUC Matrix
                hp_perf_mat[i,j,k,l] = auc_i
                # Print each iteration and AUC
                print(str(p_learning_rate[i])+'-'+str(p_max_depth[j])+'-'+str(p_num_leaves[k])+'-'+str(p_num_iterations[l])+'-'+str(auc_i))
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-2-500-0.682323491179265


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-2-600-0.6846523464346049


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-2-700-0.6864428988155052


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-10-500-0.6980602092511616


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-10-600-0.6989782671492994


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-10-700-0.6996211562446094


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-20-500-0.6980602092511616


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-20-600-0.6989782671492994


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-3-20-700-0.6996211562446094


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-2-500-0.682323491179265


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-2-600-0.6846523464346049


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-2-700-0.6864428988155052


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-10-500-0.7016923493164375


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-10-600-0.702014647375958


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-10-700-0.7021074940652048


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-20-500-0.7016858720384207


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-20-600-0.701378098654506


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-9-20-700-0.7010069771493268


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-2-500-0.682323491179265


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-2-600-0.6846523464346049


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-2-700-0.6864428988155052


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-10-500-0.7016923493164375


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-10-600-0.702014647375958


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-10-700-0.7021074940652048


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-20-500-0.7017927471256972


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-20-600-0.7016304001845468


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.05-15-20-700-0.7012349482433828


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-2-500-0.6847467931912086


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-2-600-0.6868246945668215


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-2-700-0.688380824245179


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-10-500-0.6991484860795851


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-10-600-0.6998237743997437


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-10-700-0.70030532194382


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-20-500-0.6991484860795851


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-20-600-0.6998237743997437


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-3-20-700-0.70030532194382


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-2-500-0.6847467931912086


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-2-600-0.6868246945668215


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-2-700-0.688380824245179


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-10-500-0.702449750184136


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-10-600-0.7028219113052716


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-10-700-0.7026356959796725


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-20-500-0.7011454385930085


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-20-600-0.7007648321965725


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-9-20-700-0.7004062502566952


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-2-500-0.6847467931912086


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-2-600-0.6868246945668215


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-2-700-0.688380824245179


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-10-500-0.702449750184136


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-10-600-0.7028219113052716


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-10-700-0.7026356959796725


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-20-500-0.7012352691125051


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-20-600-0.700599931137208


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.06-15-20-700-0.7002181011598521


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-2-500-0.6866260808583343


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-2-600-0.6883486303765686


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-2-700-0.6896375873106272


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-10-500-0.6999067340417667


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-10-600-0.7007785653950095


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-10-700-0.7010828263315984


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-20-500-0.6999067340417667


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-20-600-0.7007785653950095


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-3-20-700-0.7010828263315984


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-2-500-0.6866260808583343


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-2-600-0.6883486303765686


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-2-700-0.6896375873106272


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-10-500-0.7021778328550815


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-10-600-0.7021756252755196


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-10-700-0.702072412374493


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-20-500-0.700772558725039


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-20-600-0.7003398389048762


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-9-20-700-0.6997209679329389


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-2-500-0.6866260808583343


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-2-600-0.6883486303765686


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-2-700-0.6896375873106272


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-10-500-0.7021778328550815


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-10-600-0.7021756252755196


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-10-700-0.702072412374493


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-20-500-0.7006438902069716


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-20-600-0.7004034608344583


    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


    0.07-15-20-700-0.6998457517954981
    CPU times: user 11min 13s, sys: 1min 59s, total: 13min 12s
    Wall time: 3min 38s


## Best hiperparameters

Create a DataFrame with all the posible combinations of hiperparameters


```python
# Number of iterations
it=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                it=it+1
```

Create a schema dataframe with zeros


```python
auc_data = np.zeros(shape=(it,5))
auc_data=pd.DataFrame(auc_data,columns=['learn_rate',
                                        'max_depth',
                                        'num_leaves',
                                        'num_iterations',
                                        'AUC'])
```

Fill the DataFrame with the combinations of hiperparameters


```python
it_row=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                auc_data['learn_rate'][it_row]=p_learning_rate[i]
                auc_data['max_depth'][it_row]=p_max_depth[j]
                auc_data['num_leaves'][it_row]=p_num_leaves[k]
                auc_data['num_iterations'][it_row]=p_num_iterations[l]
                it_row=it_row+1
```

Fill the data frame with the AUC for each combination of hiperparameters


```python
it_row=0
for i in range(p_learning_rate.shape[0]):
    for j in range(p_max_depth.shape[0]):
        for k in range(p_num_leaves.shape[0]):
            for l in range(p_num_iterations.shape[0]):
                auc_data['AUC'][it_row]=hp_perf_mat[i,j,k,l]
                it_row=it_row+1
```

### AUC Plot - Grid Hiperparameters


```python
auc_data['Grid']=auc_data['learn_rate'].astype('str')+'-'+auc_data['max_depth'].astype('str')+'-'+auc_data['num_leaves'].astype('str')+'-'+auc_data['num_iterations'].astype('str')
```


```python
(
auc_data >>
    ggplot() +
    geom_line(aes(x = 'Grid', y = 'AUC', group = 1), colour='darkblue') +
    geom_point(aes(x = 'Grid', y = 'AUC'), colour='darkblue', size = 0.2) +
    ggtitle('AUC in Test - Grid \n Learn_rate,Max_depth,Num_leaves,Num_iterations') +
    theme_bw() +
    theme(axis_text_x = element_text(angle = 90,hjust=1,size = 4))
)
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:517: MatplotlibDeprecationWarning: isinstance(..., numbers.Number)
      return not cbook.iterable(value) and (cbook.is_numlike(value) or



![png](output_288_1.png)





    <ggplot: (-9223372029301222077)>



### Optimal Hiperparameters


```python
auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>learn_rate</th>
      <th>max_depth</th>
      <th>num_leaves</th>
      <th>num_iterations</th>
      <th>AUC</th>
      <th>Grid</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>40</th>
      <td>0.06</td>
      <td>9.0</td>
      <td>10.0</td>
      <td>600.0</td>
      <td>0.702822</td>
      <td>0.06-9.0-10.0-600.0</td>
    </tr>
    <tr>
      <th>49</th>
      <td>0.06</td>
      <td>15.0</td>
      <td>10.0</td>
      <td>600.0</td>
      <td>0.702822</td>
      <td>0.06-15.0-10.0-600.0</td>
    </tr>
  </tbody>
</table>
</div>



Save the optimal hiperparameters in objetcs


```python
opt_learn_rate=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['learn_rate'][:1]
opt_max_depth=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['max_depth'][:1]
opt_num_leaves=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_leaves'][:1]
opt_num_iterations=auc_data[auc_data['AUC']==auc_data['AUC'].max(0)]['num_iterations'][:1]
```

## Model With Optimal Metaparameters


```python
params_opt = {}
params_opt['learning_rate'] = float(opt_learn_rate)
params_opt['boosting_type'] = 'gbdt'
params_opt['objective'] = 'binary'
params_opt['metric'] = 'binary_logloss'
params_opt['sub_feature'] = 0.75
params_opt['num_leaves'] = int(float(opt_num_leaves))
params_opt['min_data'] = 200
params_opt['max_depth'] = int(float(opt_max_depth))
params_opt['num_iterations'] = int(float(opt_num_iterations))
params_opt['num_threads'] = 5
params_opt['tree_learner'] = 'data'
params_opt['seed'] = 52
params_opt['lambda_l1'] = 0.5
clf_opt = lgb.train(params_opt, d_train) 
```

    /Users/darias/anaconda3/lib/python3.7/site-packages/lightgbm/engine.py:116: UserWarning: Found `num_iterations` in params. Will use it instead of argument
      warnings.warn("Found `{}` in params. Will use it instead of argument".format(alias))


## Accuracy

Function that calculates for different thresholds:
 - Precision
 - Recall
 - F1 score


```python
def prec_rec(model, features, y, threshold):
    y_est_1=model.predict(features)
    for i in range(0,len(features)):
        if y_est_1[i]>=threshold:
            y_est_1[i]=1
        else:
            y_est_1[i]=0
    
    aux_1=(
        y.to_frame() >>
        mutate(y_pred = y_est_1)
    )
    aux_1.columns = ['y_obs', 'y_est']
    tp_1 = sum(np.where((aux_1['y_obs'] == 1)  & (aux_1['y_est'] == 1), 1, 0))
    fp_1 = sum(np.where((aux_1['y_obs'] == 0)  & (aux_1['y_est'] == 1), 1, 0))
    fn_1 = sum(np.where((aux_1['y_obs'] == 1)  & (aux_1['y_est'] == 0), 1, 0))
    tn_1 = sum(np.where((aux_1['y_obs'] == 0)  & (aux_1['y_est'] == 0), 1, 0))
    precision_1 = tp_1/(tp_1+fp_1)
    recall_1 = tp_1/(tp_1+fn_1)
    f1_score_1=1/(((1/recall_1)+(1/precision_1))/2)
    y_est_0=model.predict(features)
    for i in range(0,len(features)):
        if y_est_0[i]<threshold:
           y_est_0[i]=1
        else:
           y_est_0[i]=0

    aux_0=(
    y.to_frame() >>
        mutate(y_pred = y_est_1)
    )
    aux_0.columns = ['y_obs', 'y_est']
    tp_0 = sum(np.where((aux_0['y_obs'] == 0)  & (aux_0['y_est'] == 0), 1, 0))
    fp_0 = sum(np.where((aux_0['y_obs'] == 1)  & (aux_0['y_est'] == 0), 1, 0))
    fn_0 = sum(np.where((aux_0['y_obs'] == 0)  & (aux_0['y_est'] == 1), 1, 0))
    tn_0 = sum(np.where((aux_0['y_obs'] == 1)  & (aux_0['y_est'] == 1), 1, 0))
    precision_0 = tp_0/(tp_0+fp_0)
    recall_0 = tp_0/(tp_0+fn_0)
    f1_score_0=1/(((1/recall_0)+(1/precision_0))/2)
    return(f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0)
```

### Test


```python
auc_opt_t = roc_auc_score(ytest,clf_opt.predict(X_test))
auc_opt_t
```




    0.7028219113052716



### Validation


```python
y_pred_opt_v=clf_opt.predict(Xv)
for i in range(0,len(Xv)):
    if y_pred_opt_v[i]>=.5:
       y_pred_opt_v[i]=1
    else:  
       y_pred_opt_v[i]=0
```


```python
# Confussion Matrix
cm_opt_v = confusion_matrix(y_pred_opt_v,yv.values)

# Accuracy
accuracy_opt_v=accuracy_score(y_pred_opt_v,yv.values)

# AUC
auc_opt_v = roc_auc_score(yv,clf_opt.predict(Xv))
```


```python
cm_opt_v
```




    array([[9990, 5036],
           [4536, 7546]])




```python
accuracy_opt_v
```




    0.6468939058580493




```python
auc_opt_v
```




    0.7008168313153336




```python
threshold = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
threshold_data=pd.DataFrame(index=['F1 - Class 1', 'F1 - Class 0',
                    'Precision - Class 1', 'Precision - Class 0',
                    'Recall - Class 1', 'Recall - Class 0'],
             columns=['Threshold:0.1','Threshold:0.2','Threshold:0.3','Threshold:0.4',
                      'Threshold:0.5','Threshold:0.6','Threshold:0.7','Threshold:0.8',
                      'Threshold:0.9'])
for i in threshold:
    f1_score_1,f1_score_0,precision_1,precision_0,recall_1,recall_0=prec_rec(clf_opt, Xv, yv, i)
    threshold_data.loc['F1 - Class 1','Threshold:'+str(i)]=f1_score_1
    threshold_data.loc['F1 - Class 0','Threshold:'+str(i)]=f1_score_0
    threshold_data.loc['Precision - Class 1','Threshold:'+str(i)]=precision_1
    threshold_data.loc['Precision - Class 0','Threshold:'+str(i)]=precision_0
    threshold_data.loc['Recall - Class 1','Threshold:'+str(i)]=recall_1
    threshold_data.loc['Recall - Class 0','Threshold:'+str(i)]=recall_0
```


```python
threshold_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Threshold:0.1</th>
      <th>Threshold:0.2</th>
      <th>Threshold:0.3</th>
      <th>Threshold:0.4</th>
      <th>Threshold:0.5</th>
      <th>Threshold:0.6</th>
      <th>Threshold:0.7</th>
      <th>Threshold:0.8</th>
      <th>Threshold:0.9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>F1 - Class 1</th>
      <td>0.634078</td>
      <td>0.637394</td>
      <td>0.656458</td>
      <td>0.661417</td>
      <td>0.611904</td>
      <td>0.497595</td>
      <td>0.359613</td>
      <td>0.195532</td>
      <td>0.0177827</td>
    </tr>
    <tr>
      <th>F1 - Class 0</th>
      <td>0.000550585</td>
      <td>0.0398331</td>
      <td>0.318362</td>
      <td>0.539566</td>
      <td>0.676096</td>
      <td>0.716848</td>
      <td>0.721038</td>
      <td>0.712372</td>
      <td>0.699256</td>
    </tr>
    <tr>
      <th>Precision - Class 1</th>
      <td>0.464212</td>
      <td>0.468475</td>
      <td>0.504219</td>
      <td>0.553698</td>
      <td>0.624565</td>
      <td>0.698563</td>
      <td>0.764539</td>
      <td>0.822628</td>
      <td>0.889764</td>
    </tr>
    <tr>
      <th>Precision - Class 0</th>
      <td>1</td>
      <td>0.880952</td>
      <td>0.794069</td>
      <td>0.733665</td>
      <td>0.664848</td>
      <td>0.616835</td>
      <td>0.585869</td>
      <td>0.559797</td>
      <td>0.53786</td>
    </tr>
    <tr>
      <th>Recall - Class 1</th>
      <td>1</td>
      <td>0.996821</td>
      <td>0.940391</td>
      <td>0.821173</td>
      <td>0.599746</td>
      <td>0.386425</td>
      <td>0.235098</td>
      <td>0.110952</td>
      <td>0.00898108</td>
    </tr>
    <tr>
      <th>Recall - Class 0</th>
      <td>0.000275368</td>
      <td>0.0203773</td>
      <td>0.199091</td>
      <td>0.426683</td>
      <td>0.687732</td>
      <td>0.855569</td>
      <td>0.937285</td>
      <td>0.979279</td>
      <td>0.999036</td>
    </tr>
  </tbody>
</table>
</div>



#### ROC


```python
fpr, tpr, thresholds = roc_curve(yv, clf_opt.predict(Xv))
df=pd.DataFrame(dict(fpr = fpr, tpr = tpr))
(
df >>
    ggplot(aes(x = 'fpr', y = 'tpr')) +
    geom_line(colour = 'darkblue') +
    geom_abline(linetype = 'dashed') +
    theme_bw() +
    ggtitle('ROC CURVE')
)
```


![png](output_309_0.png)





    <ggplot: (-9223372029121192290)>



## Feature Importance


```python
#lgb.plot_importance(clf_opt,max_num_features=10, title='Feature Importance - Top 10')
var_im = pd.DataFrame(sorted(zip(clf_opt.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature']).head(10)
(
 var_im >>
    ggplot() +
    geom_bar(aes(x = 'Feature', y = 'Value'), stat = 'identity', fill = 'darkblue') +
    scale_x_discrete(limits = var_im.sort_values(['Value'],ascending=True)['Feature']) +
    coord_flip() +
    ggtitle('Feature Importance') +
    scale_y_continuous(labels = comma_format()) +
    theme_bw() +
    theme(axis_text_x  = element_text(size = 15),
          axis_text_y  = element_text(size = 15),
          axis_title_x = element_text(size = 15),
          axis_title_y = element_text(size = 15),
          title        = element_text(size = 15))
)
```


![png](output_311_0.png)





    <ggplot: (-9223372029301090437)>



## Partial Plots - Top 5

Function that plot a marginal effect


```python
def partial_plot(model,frame,feature):
    df_aux=frame
    df_aux[feature]=Xv[feature].quantile(
        q=[0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55,
           0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99]
       ).values
    df_aux['mean_response']=model.predict(df_aux)
    pplot=(
        df_aux >>
        distinct(feature) >>
        ggplot() +
        ggtitle(feature+' - Partial Plot') +
        theme_bw() +
        scale_y_continuous(labels = percent_format()) +
        geom_line(aes(x = feature, y = 'mean_response'), colour='darkblue')
        )
#    if len(df_aux[feature].unique()) < 40:
#        pplot=(
#        df_aux >>
#        distinct(X.num_store_Mexico) >>
#        ggplot() +
#        ggtitle(feature+' - Partial Plot') +
#        theme_bw() +
#        scale_y_continuous(labels = percent_format()) +
#        geom_line(aes(x = feature, y = 'mean_response'), colour='darkblue')
#        )
#    else:
#        pplot=(
#        df_aux >>
#        ggplot() +
#        ggtitle(feature+' - Partial Plot') +
#        theme_bw() +
#        scale_y_continuous(labels = percent_format()) +
#        geom_smooth(aes(x = feature, y = 'mean_response'), colour='darkblue')
#        )
    return(pplot)
```

Df with the mean of all features to create the marginal effects


```python
avg_frame_opt=Xv.mean(axis=0).to_frame().T
avg_frame_opt_20=pd.DataFrame(np.repeat(avg_frame_opt.values,21,axis=0))
avg_frame_opt_20.columns = avg_frame_opt.columns
```

Top 5 features in a vector


```python
fi_opt = pd.DataFrame(sorted(zip(clf_opt.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])
top10_opt=(
fi_opt >>
    select('Feature') >>
    head(10) >>
    pull
)
```


```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[0])
```


![png](output_319_0.png)





    <ggplot: (7551384626)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[1])
```


![png](output_320_0.png)





    <ggplot: (297537782)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[2])
```


![png](output_321_0.png)





    <ggplot: (7551912396)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[3])
```


![png](output_322_0.png)





    <ggplot: (7555123383)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[4])
```


![png](output_323_0.png)





    <ggplot: (-9223372029303433079)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[5])
```


![png](output_324_0.png)





    <ggplot: (-9223372036557056533)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[6])
```


![png](output_325_0.png)





    <ggplot: (-9223372036557021973)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[7])
```


![png](output_326_0.png)





    <ggplot: (-9223372029303392466)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[8])
```


![png](output_327_0.png)





    <ggplot: (-9223372036556992936)>




```python
partial_plot(clf_opt,avg_frame_opt_20,top10_opt[9])
```


![png](output_328_0.png)





    <ggplot: (-9223372029303499162)>



## Efectiveness

Back testing in the _validation_ frame


```python
predict_ = pd.DataFrame(clf_opt.predict(Xv), columns=['predict'])

predict_efect = pd.DataFrame([predict_.predict.values, yv.values]).T

predict_efect.columns = ['predict', 'y_red']

predict_efect['r_score'] = pd.qcut(predict_efect.predict, q=[0.01, 0.1,0.2,0.3,0.4,0.5,0.6,
                                                         0.7,0.8,0.9,1.0], 
                                   labels=['q10%','q20%','q30%','q40%','q50%','q60%','q70%','q80%','q90%','q100%'])

a = predict_efect.loc[predict_efect.y_red == 1].groupby(['r_score']).y_red.count() /predict_efect.groupby(['r_score']).y_red.count()

a.plot.bar()
plt.title('Backtest Efectiveness',fontsize=20)
plt.legend(['Efectiveness'])
plt.xticks(fontsize=10)
plt.plot([-1,100],[a.mean(),a.mean()],color='blue')
plt.text(0.2,0.7,'Mean Efectiveness')
for i in range(len(a)):
    plt.text(i-0.5, a[i], str((a[i]*100).round(2))+'%')
```


![png](output_331_0.png)


## Non used features

Features with importance = 0 for the _LightGBM_


```python
nu = pd.DataFrame(sorted(zip(clf_opt.feature_importance(),X.columns),reverse=True), 
                               columns=['Value','Feature'])

nu[nu['Value']==0]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Value</th>
      <th>Feature</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>107</th>
      <td>0</td>
      <td>total_loans_0</td>
    </tr>
    <tr>
      <th>108</th>
      <td>0</td>
      <td>night</td>
    </tr>
    <tr>
      <th>109</th>
      <td>0</td>
      <td>Zacatecas</td>
    </tr>
    <tr>
      <th>110</th>
      <td>0</td>
      <td>Yucatan</td>
    </tr>
    <tr>
      <th>111</th>
      <td>0</td>
      <td>Tamaulipas</td>
    </tr>
    <tr>
      <th>112</th>
      <td>0</td>
      <td>Sonora</td>
    </tr>
    <tr>
      <th>113</th>
      <td>0</td>
      <td>San Luis Potosi</td>
    </tr>
    <tr>
      <th>114</th>
      <td>0</td>
      <td>Queretaro</td>
    </tr>
    <tr>
      <th>115</th>
      <td>0</td>
      <td>Quantity_Based_Items</td>
    </tr>
    <tr>
      <th>116</th>
      <td>0</td>
      <td>Oaxaca</td>
    </tr>
    <tr>
      <th>117</th>
      <td>0</td>
      <td>Nayarit</td>
    </tr>
    <tr>
      <th>118</th>
      <td>0</td>
      <td>Guerrero</td>
    </tr>
    <tr>
      <th>119</th>
      <td>0</td>
      <td>Firearms</td>
    </tr>
    <tr>
      <th>120</th>
      <td>0</td>
      <td>Earrings</td>
    </tr>
    <tr>
      <th>121</th>
      <td>0</td>
      <td>Durango</td>
    </tr>
    <tr>
      <th>122</th>
      <td>0</td>
      <td>Colima</td>
    </tr>
    <tr>
      <th>123</th>
      <td>0</td>
      <td>Chihuahua</td>
    </tr>
    <tr>
      <th>124</th>
      <td>0</td>
      <td>Chiapas</td>
    </tr>
    <tr>
      <th>125</th>
      <td>0</td>
      <td>Bulks</td>
    </tr>
    <tr>
      <th>126</th>
      <td>0</td>
      <td>Baja California Sur</td>
    </tr>
    <tr>
      <th>127</th>
      <td>0</td>
      <td>Baja California</td>
    </tr>
    <tr>
      <th>128</th>
      <td>0</td>
      <td>Aguascalientes</td>
    </tr>
  </tbody>
</table>
</div>



## Save Model


```python
output=open('lgbm.pkl', 'wb')
pickle.dump(clf_opt, output)
output.close()
```

---------------------


```python

```


```python
#0.06-9.0-10.0-600.0
p_learning_rate = np.array([0.05,0.06,0.07])
p_max_depth = np.array([3,9,15]) 
p_num_leaves = np.array([2,10,20])
p_num_iterations = np.array([500,600,700])
```

# Another war to optimize metaparameters


```python
#gkf = KFold(n_splits=2,
#            shuffle=True,
#            random_state=42).split(X=X_data_downsampled,
#                                   y=y_data_downsampled)
```


```python
#0.06-9.0-10.0-600.0
#param_grid = {
#    'learning_rate': np.array([0.05, 0.06]),
#    'num_leaves': np.array([2,10]),
#    'reg_alpha': [0, 0.5],
#    'min_data_in_leaf': [200,300],
#    'lambda_l1': [0,1],
#    'lambda_l2': [0, 1],
#    'num_iterations': np.array([500,600])
#    }
```


```python
#lgb_estimator = lgb.LGBMClassifier(boosting_type='gbdt',
#                                   objective='binary',
#                                   num_boost_round=100,
#                                   metric='binary_logloss')
```


```python
#gsearch = GridSearchCV(estimator=lgb_estimator,
#                       param_grid=param_grid,
#                       cv=gkf)
```


```python
#lgb_model = gsearch.fit(X=X_data_downsampled,
#                        y=y_data_downsampled)
```


```python
#print(lgb_model.best_params_, lgb_model.best_score_)
```

    {'lambda_l1': 1, 'lambda_l2': 0, 'learning_rate': 0.06, 'min_data_in_leaf': 200, 'num_iterations': 600, 'num_leaves': 10, 'reg_alpha': 0} 0.6426897377081879

